import { combineReducers, createStore } from 'redux'
import customerReducer from './customerReducer'
import configReducer from './configReducer';

const store = createStore(
    combineReducers({
        customer: customerReducer,
        config: configReducer,
    }))
export default store
