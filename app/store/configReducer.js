const config = {
    country: {},
};

const configReducer = (state = config, action) => {
    switch (action.type) {
        case 'SET_COUNTRY':
            return {
                ...state,
                ...action.value,
            };
        default:
            break;
    }
    return state;
};
export default configReducer;
