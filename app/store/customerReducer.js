const user = {

}

const customerReducer = (state = user, action) => {
    switch (action.type) {
        case 'SET_CUSTOMER':
            return {
                ...state,
                ...action.value
            }
        default: break;
    }
    return state;
}
export default customerReducer
