import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

import { Constants } from '../constants'

export default class API {
    async getToken() {
        try {
            let data = await AsyncStorage.getItem('token')
            return data
        } catch (error) {
            console.log(error);
        }
    }

    async me(token) {
        try {
            let response = await axios.get(`${Constants.baseUrl}/api/auth/me`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return response
        } catch (e) {
            console.log(e);
        }
    }

    async signup(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/signup`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    console.log(err, 'res err');
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async signupchurch(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/signup/church`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    console.log(err.response);
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async signin(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/signin`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async signin_google(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/google/signin`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async signin_face(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/face/signin`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async reset_password_email(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/reset/password/email`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async reset_password_code(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/reset/password/code`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async reset_password_change(data) {
        try {
            let responce = axios.post(`${Constants.baseUrl}/api/auth/reset/password/change`, data)
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async getChurches() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/church/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async getInvitationChurches() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/church/invitation/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async invitationChurches(id) {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/invitation/add/${id}`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async invitationRemoveChurches(id) {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/invitation/remove/${id}`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async changeProfile(data) {
        let token = await this.getToken()
        let formData = new FormData()
        formData.append('photo_customer', {
            uri: data.photo,
            type: 'image/jpeg',
            name: `filename.jpg`,
        });
        formData.append('first_name', data.firstName);
        formData.append('last_name', data.lastName);
        formData.append('phone', data.phone);
        formData.append('email', data.email);
        formData.append('country', data.country);
        try {
            let responce = axios.put(`${Constants.baseUrl}/profile/change`, formData, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async changeChurchProfile(data) {
        let token = await this.getToken()
        let formData = new FormData()
        formData.append('photo_church', {
            uri: data.photo,
            type: 'image/jpeg',
            name: `filename.jpg`,
        });
        formData.append('church_name', data.church_name);
        formData.append('phone', data.phone);
        formData.append('email', data.email);
        formData.append('country', data.country);
        try {
            let responce = axios.post(`${Constants.baseUrl}/profile/church/change`, formData, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    console.log(err);
                    console.log(err.response);
                    return err.response
                })
            return responce
        } catch (err) {
            console.log(err);
        }
    }

    async getPost() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/post/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async getChurchPost() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/post/church/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async deletePost(id) {
        let token = await this.getToken()
        try {
            let responce = axios.delete(`${Constants.baseUrl}/post/delete/${id}`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }


    async getFollowers() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/church/followers/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async getMembers() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/church/members/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async getMembers() {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/church/members/get`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async addMembers(id) {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/church/members/add/${id}`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e);
        }
    }

    async addPost(data) {
        let token = await this.getToken()
        let formData = new FormData()
        data.gallary.forEach((data, i) => {
            if (data.type === 'video') {
                formData.append("post", {
                    uri: data.uri,
                    type: '*/*',
                    name: `filename${i}.mp4`,
                });
            }
            if (data.type === 'photo') {
                formData.append("post", {
                    uri: data.uri,
                    type: "image/jpg",
                    name: `filename${i}.jpg`,
                });
            }
        });
        formData.append('description', data.description);
        try {
            let responce = axios.post(`${Constants.baseUrl}/post/add`, formData, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    console.log(err, 'ekfjkdd');
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e, 'ewf');
        }
    }

    async editPost(data) {
        let token = await this.getToken()
        let formData = new FormData()
        data.gallary.forEach((data, i) => {
            if (data.type === 'video' && data.local === true) {
                formData.append("post", {
                    uri: data.path,
                    type: '*/*',
                    name: `filename${i}.mp4`,
                });
            }
            if (data.type === 'photo' && data.local === true) {
                formData.append("post", {
                    uri: data.path,
                    type: "image/jpg",
                    name: `filename${i}.jpg`,
                });
            }
        });
        formData.append('description', data.description);
        formData.append('post_id', data.post_id);
        formData.append('delete_id', JSON.stringify(data.delete_id));
        try {
            let responce = axios.post(`${Constants.baseUrl}/post/edit`, formData, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    console.log(err, 'ekfjkdd');
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e, 'ewf');
        }
    }

    async getChat(id) {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/chat/customers`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    console.log(err, 'ekfjkdd');
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e, 'ewf');
        }
    }

    async getChatCustomers(id) {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/chat/customers/get/${id}`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e, 'ewf');
        }
    }

    async readChat(id) {
        let token = await this.getToken()
        try {
            let responce = axios.get(`${Constants.baseUrl}/chat/read/${id}`, {
                headers: {
                    'x-access-token': token
                }
            })
                .then(data => {
                    return data
                })
                .catch(err => {
                    return err.response
                })
            return responce
        } catch (e) {
            console.log(e, 'ewf');
        }
    }

}
