import io from 'socket.io-client';
import { Constants } from "../constants";

export let socket = null

export let connectSocket = (customer) =>  {
    if (socket === null) {
        socket = io.connect(`${Constants.baseUrl}`);
        socket.emit('joinRoom', {
            user_id: customer.id,
            user_name: `${customer.first_name} ${customer.last_name}`,
            email: customer.email,
            role: customer.role,
            room: customer.token
        });
    } else {
        console.log('ALREADY CONNECTED !');
    }
}

