import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import API from '../../networking/api';
import Back from '../../assets/img/back.png';
import {Constants} from "../../constants";
import {Loading} from "../";

class CustomerChatClass extends Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            loading: true,

            customers: [],
        };
        this.getChatCustomers()
    }

    getChatCustomers() {
        this.api.getChatCustomers(this.props.customer.member_church_id)
            .then(data => {
                this.setState({
                    loading: false
                })
               if(data.status === 200){
                   this.setState({
                       customers: data.data.customers
                   })
               }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err);
            })
    }

    _renderCustomersList() {
        return this.state.customers.map((data, index) => {
            return (
                <TouchableOpacity key={index} style={styles.customer_view} onPress={() => {
                   this.props.action(data)
                }}>
                    <View style={styles.customer_name_view}>
                        { data.photo ?
                                <Image source={{uri: `${Constants.baseUrl}/${data.photo}`}}
                                       style={{width: 50, height: 50, borderRadius: 50}}/>
                                :
                                <Image source={require('../../assets/img/user1.png')}
                                       style={{width: 50, height: 50, borderRadius: 50}}/>
                        }
                        <View style={styles.name_view}>
                            <Text style={styles.name_text}>
                                {data.first_name} {data.last_name}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            );
        });
    }

    render() {
        return (
            <Modal
                onSwipeComplete={() => {
                    this.props.close();
                }}
                swipeDirection="down"
                onBackButtonPress={() => {
                    this.props.close();
                }}
                backdropColor={'#fff'}
                backdropOpacity={1}
                style={{margin: 0}}
                isVisible={this.props.isVisible}>
                <View style={styles.content}>
                    <View style={styles.swipeItem}/>
                    <View style={styles.headerRow}>
                        <TouchableOpacity
                            style={styles.backButtonContainer}
                            activeOpacity={0.8}
                            onPress={() => this.props.close()}>
                            <Image
                                style={styles.backIcon}
                                source={Back}/>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>
                            Member
                        </Text>
                        <View style={{width: 38}}/>
                    </View>
                    {this._renderCustomersList()}
                </View>
                <Loading loading={this.state.loading} />
            </Modal>
        );
    }
}

export const CustomerChat = connect(({customer}) => ({customer}))(CustomerChatClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    swipeItem: {
        backgroundColor: '#B5B3BD',
        height: 3,
        width: 39,
        borderRadius: 1.5,
        alignSelf: 'center',
        marginTop: 15
    },
    backButtonContainer: {
        marginLeft: 10,
        height: 28,
        width: 28,
        justifyContent: "center",
        alignItems: "center"
    },
    backIcon: {
        height: 16,
        width: 16
    },
    headerRow: {
        marginBottom: 20,
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20
    },
    headerTitle: {
        color: 'black',
        fontFamily: 'Circular-Std-Bold',
        fontSize: 20,
        lineHeight: 22
    },
    customer_view: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
    },
    customer_name_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    name_view: {
        marginLeft: 17
    },
    name_text: {
        color: '#000000',
        fontSize: 12,
        lineHeight: 16,
        fontFamily: 'SegoeUI-Bold',
    }
});
