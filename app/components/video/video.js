import React from 'react';
import {
    StyleSheet,
    Image,
    View,
    TouchableOpacity,
} from 'react-native';
import VideoPlayer from 'react-native-video';
import {Constants} from "../../constants";


class VideoClass extends React.Component {
    render() {
        return (
            <TouchableOpacity style={{width: 100, height: 100, borderWidth: 0.5, borderColor: '#EFEFEF'}} onPress={() => this.props.play(this.props.index)}>
                <VideoPlayer source={{uri: `${Constants.baseUrl}/${this.props.uri}`}}
                             ref={(ref) => {
                                 this.player = ref
                             }}
                             onBuffer={this.onBuffer}
                             paused={this.props.paused}
                             style={styles.backgroundVideo}
                />
                {this.props.paused ?
                    <View style={styles.play_btn} >
                        <Image source={require('../../assets/img/play.png')} style={{width: 25, height: 25}}/>
                    </View>
                    :
                    null
                }

            </TouchableOpacity>
        );
    }
}

export const Video = VideoClass;

const styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    play_btn: {
        alignItems: 'center',
        marginVertical: 37
    },
    delete_btn: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#fff',
        borderRadius: 50,
        width: 25,
        height: 25,
        right: -10,
        top: -5
    }
});


