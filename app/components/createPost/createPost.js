import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
    TextInput,
    ScrollView
} from 'react-native';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import API from '../../networking/api';
import Back from '../../assets/img/back.png';
import {Video} from '../index'
import {Constants} from "../../constants";

class CreatePostClass extends Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            gallary: [],
            description:  '',

            err: false
        };
    }

    handleChange = (e, name) => {
        this.setState({
            [name]: e,
            err: false,
        });
    };

    addPost() {
        if (this.state.description) {
            let data = {
                description: this.state.description,
                gallary: this.state.gallary
            }
            this.setState({
                loading: true
            })
            this.api.addPost(data)
                .then(data => {
                    if(data.status === 200) {
                        this.setState({
                            gallary: [],
                            description: '',
                        })
                        this.props.add()
                    }
                })
                .catch(err => {
                    this.setState({
                        loading: false
                    })
                    console.log(err);
                })
        } else {
            this.setState({
                err: true
            })
        }
    }

    selectPhoto() {
        const options = {
            title: 'Select Poto',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let arr = this.state.gallary
                const source = {uri: response.uri, type: 'photo'};
                arr.push(source)
                this.setState({
                    gallary: arr
                })
            }
        });
    }

    selectVideo() {
        const options = {
            mediaType: 'video',
            title: 'Select Video',
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let arr = this.state.gallary
                const source = {uri: response.uri, type: 'video'};
                arr.push(source)
                this.setState({
                    gallary: arr
                })

            }
        });
    }

    _renderVideoPhoto(gallary) {
        return gallary.map((data, index) => {
            if (data.type === 'photo') {
                return <Image source={{uri: data.uri}} style={{width: 100, height: 100, marginTop: 10, marginRight: 10}}
                              key={index}/>
            }
            if (data.type === 'video') {
                return (
                    <View style={{width: 100, height: 100, marginTop: 10, marginRight: 10}}>
                        <Video uri={data.uri} key={index} paused={true}/>
                    </View>
                )
            }
        })
    }

    render() {
        return (
            <Modal
                onSwipeComplete={() => {
                    this.props.close();
                }}
                onBackButtonPress={() => {
                    this.props.close();
                }}
                backdropColor={'#fff'}
                backdropOpacity={1}
                style={{margin: 0}}
                isVisible={this.props.isVisible}>
                <View style={styles.content}>
                    <ScrollView>
                        <View style={styles.headerRow}>
                            <TouchableOpacity
                                style={styles.backButtonContainer}
                                activeOpacity={0.8}
                                onPress={() => this.props.close()}>
                                <Image
                                    style={styles.backIcon}
                                    source={Back}/>
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}>
                                Creat Post
                            </Text>
                            <View style={{width: 38}}/>
                        </View>
                        <View style={styles.church_info}>
                            <Image source={require('../../assets/img/churchAvatar.png')}
                                   style={{width: 45, height: 45, borderRadius: 50}}/>
                            <Text style={styles.church_name}>{this.props.customer.church_name}</Text>
                        </View>
                        <View style={styles.textAreaContainer}>
                            <TextInput
                                style={styles.textArea}
                                onChangeText={(e) => this.handleChange(e, 'description')}
                                underlineColorAndroid="transparent"
                                placeholder="Type something"
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                multiline={true}
                            />
                        </View>
                        <TouchableOpacity style={styles.video_view} onPress={() => this.selectPhoto()}>
                            <Image source={require('../../assets/img/video.png')} style={{width: 23, height: 23}}/>
                            <Text style={styles.video_text}>Photo</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.video_view} onPress={() => this.selectVideo()}>
                            <Image source={require('../../assets/img/video.png')} style={{width: 23, height: 23}}/>
                            <Text style={styles.video_text}>Video</Text>
                        </TouchableOpacity>
                        <View style={styles.gallary_view}>
                            {this._renderVideoPhoto(this.state.gallary)}
                        </View>
                        <View style={{height: 20}}>
                            {this.state.err ?
                                <Text style={styles.err_text}>Please enter description</Text>
                                :
                                null
                            }
                        </View>
                        <View style={styles.btn_view}>
                            <TouchableOpacity style={styles.btn} onPress={() => this.addPost()}>
                                <Text style={styles.btn_text}>Post</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.btn, {backgroundColor: '#D5D5D5'}]}>
                                <Text style={styles.btn_text}>Delete</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        );
    }
}

export const CreatePost = connect(({customer}) => ({customer}))(CreatePostClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    swipeItem: {
        backgroundColor: '#B5B3BD',
        height: 3,
        width: 39,
        borderRadius: 1.5,
        alignSelf: 'center',
        marginTop: 15
    },
    backButtonContainer: {
        marginLeft: 10,
        height: 28,
        width: 28,
        justifyContent: "center",
        alignItems: "center"
    },
    backIcon: {
        height: 16,
        width: 16
    },
    headerRow: {
        backgroundColor: '#FFF300',
        marginBottom: 20,
        height: 55,
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20
    },
    headerTitle: {
        color: 'black',
        fontFamily: 'Circular-Std-Bold',
        fontSize: 20,
        fontWeight: '700',
        lineHeight: 27
    },
    church_info: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 27,
    },
    church_name: {
        fontSize: 14,
        color: '#000',
        marginLeft: 10,
        lineHeight: 19,
        fontWeight: '700'
    },
    textAreaContainer: {
        marginTop: 20,
        marginHorizontal: 20,
        borderColor: '#EFEFEF',
        borderWidth: 1,
        padding: 5
    },
    textArea: {
        textAlignVertical: 'top',
        height: 350,
        justifyContent: "flex-start"
    },
    video_view: {
        marginTop: 20,
        marginLeft: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    video_text: {
        marginLeft: 10
    },
    gallary_view: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    err_text: {
        marginTop: 5,
        marginLeft: 20,
        color: 'red',
        fontSize: 14
    },
    btn_view: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginBottom: 30
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        height: 45,
        backgroundColor: '#37ADDB',
        borderRadius: 10,
    },
    btn_text: {
        fontSize: 20,
        lineHeight: 27,
        color: '#fff'
    }
});
