import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import API from '../../networking/api';
import Back from '../../assets/img/back.png';
import {Constants} from "../../constants";

class MembersClass extends Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            memebers: [],
        };
        this.getMembers()
    }

    getMembers() {
        this.api.getMembers()
            .then(data => {
                if (data.status === 200) {
                    this.setState({
                        memebers: data.data.members_churche
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    _renderMembersList() {
        return this.state.memebers.map((data, index) => {
            return (
                <TouchableOpacity key={index} style={styles.btn} onPress={() => {
                  /*  this.remove(data.id)*/
                }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        {data.photo ?
                            <Image source={{uri: `${Constants.baseUrl}/${data.photo}`}}
                                   style={styles.img_church}/>
                            :
                            <Image source={require('../../assets/img/profile.png')} style={styles.img_church}/>
                        }
                        <Text style={styles.name_text}>
                            {data.first_name} {data.last_name}
                        </Text>
                    </View>
                   {/* <Image source = {require('../../assets/img/close.png')} style={{width: 20, height: 20}}/>*/}
                </TouchableOpacity>
            )
        })
    }

    remove(id) {
        this.setState({
            loading: true
        })
        this.api.invitationRemoveChurches(id)
            .then(async invite => {
                if(invite.status === 200){
                    return this.getFollowers()
                }
                this.setState({
                    loading: false
                })
            })
            .catch(err => {
                console.log(err);
            })
    }

    render() {
        return (
            <Modal
                onSwipeComplete={() => {
                    this.props.close();
                }}
                swipeDirection="down"
                onBackButtonPress={() => {
                    this.props.close();
                }}
                backdropColor={'#fff'}
                backdropOpacity={1}
                style={{margin: 0}}
                isVisible={this.props.isVisible}>
                <View style={styles.content}>
                    <View style={styles.swipeItem}/>
                    <View style={styles.headerRow}>
                        <TouchableOpacity
                            style={styles.backButtonContainer}
                            activeOpacity={0.8}
                            onPress={() => this.props.close()}>
                            <Image
                                style={styles.backIcon}
                                source={Back}/>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>
                            Members
                        </Text>
                        <View style={{width: 38}}/>
                    </View>
                    {this._renderMembersList()}
                </View>
            </Modal>
        );
    }
}

export const Members = connect(({customer}) => ({customer}))(MembersClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    swipeItem: {
        backgroundColor: '#B5B3BD',
        height: 3,
        width: 39,
        borderRadius: 1.5,
        alignSelf: 'center',
        marginTop: 15
    },
    backButtonContainer: {
        marginLeft: 10,
        height: 28,
        width: 28,
        justifyContent: "center",
        alignItems: "center"
    },
    backIcon: {
        height: 16,
        width: 16
    },
    headerRow: {
        marginBottom: 20,
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20
    },
    headerTitle: {
        color: 'black',
        fontFamily: 'Circular-Std-Bold',
        fontSize: 20,
        lineHeight: 22
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 15,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: '#9F9F9F',
    },
    name_text: {
        marginLeft: 10,
        fontSize: 16,
        color: '#000',
        lineHeight: 20
    },
    img_church: {
        width: 40,
        height: 40,
        borderRadius: 50
    }

});
