import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import API from '../../networking/api';
import Back from '../../assets/img/back.png';
import {Constants} from "../../constants";

class ChooseChurchClass extends Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            churches: [],
        };
        this.getChurches()
    }

    getChurches() {
        this.api.getChurches()
            .then(data => {
                if (data.status === 200) {
                    this.setState({
                        churches: data.data.churches
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    _renderChurchesList() {
        return this.state.churches.map((data, index) => {
            return (
                <TouchableOpacity key={index} style={styles.btn} onPress={() => {
                    this.member(data.id)

                }}>
                    <Text style={styles.name_text}>
                        {data.name}
                    </Text>
                    {data.photo ?
                        <Image source={{uri: `${Constants.baseUrl}/${data.photo}`}} style={styles.img_church}/>
                        :
                        <Image source={require('../../assets/img/church_tab.png')} style={styles.img_church}/>
                    }
                </TouchableOpacity>
            )
        })
    }

    member(id){
        this.api.addMembers(id)
            .then(async member => {
                if(member.status === 200){
                  await  this.props.dispatch({
                        type: 'SET_CUSTOMER', value: {
                          member_church_id: member.data.church_id,
                          member_church_name: member.data.church_name,
                        },
                    });
                    this.props.close(member.data.church_name, member.data.church_id)
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    render() {
        return (
            <Modal
                onSwipeComplete={() => {
                    this.props.close();
                }}
                swipeDirection="down"
                onBackButtonPress={() => {
                    this.props.close();
                }}
                backdropColor={'#fff'}
                backdropOpacity={1}
                style={{margin: 0}}
                isVisible={this.props.isVisible}>
                <View style={styles.content}>
                    <View style={styles.swipeItem}/>
                    <View style={styles.headerRow}>
                        <TouchableOpacity
                            style={styles.backButtonContainer}
                            activeOpacity={0.8}
                            onPress={() => this.props.close()}>
                            <Image
                                style={styles.backIcon}
                                source={Back}/>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>
                            Choose your church
                        </Text>
                        <View style={{width: 38}}/>
                    </View>
                    {this._renderChurchesList()}
                </View>
            </Modal>
        );
    }
}

export const ChooseChurch = connect(({customer}) => ({customer}))(ChooseChurchClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    swipeItem: {
        backgroundColor: '#B5B3BD',
        height: 3,
        width: 39,
        borderRadius: 1.5,
        alignSelf: 'center',
        marginTop: 15
    },
    backButtonContainer: {
        marginLeft: 10,
        height: 28,
        width: 28,
        justifyContent: "center",
        alignItems: "center"
    },
    backIcon: {
        height: 16,
        width: 16
    },
    headerRow: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20
    },
    headerTitle: {
        color: 'black',
        fontFamily: 'Circular-Std-Bold',
        fontSize: 20,
        lineHeight: 22
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 15,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: '#9F9F9F',
    },
    name_text: {
        fontSize: 16,
        color: '#000',
        lineHeight: 20
    },
    img_church: {
        width: 40,
        height: 40,
        borderRadius: 50
    }

});
