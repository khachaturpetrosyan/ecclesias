import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';


class HeaderClass extends React.Component {
    render() {
        return (
            <View style={[styles.header, !this.props.back ? {justifyContent: 'center'} : null]}>
                {
                    this.props.back ?
                        <TouchableOpacity style={styles.back_btn} onPress={() => this.props.navigation.goBack()}>
                            <Image source={require('../../assets/img/back.png')} style={{width: 24, height: 24}}/>
                            <Text>back</Text>
                        </TouchableOpacity>
                        :
                        null
                }
                <Image source={require('../../assets/img/logo.png')} style={{width: 155, height: 50}}/>
                {
                    this.props.back ?
                        <Text/>
                        :
                        null
                }
            </View>
        );
    }
}

export const Header = HeaderClass;

const styles = StyleSheet.create({
    header: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        height: 55,
        backgroundColor: '#FFF300',
    },
    back_btn: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    back_text: {
        marginLeft: 6,
        fontSize: 12,
        lineHeight: 16,
        color: '#000000',
        fontFamily: 'SegoeUI',
    },
});


