import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {connect} from "react-redux";
import API from '../../../networking/api';
import {Constants} from "../../../constants";
import {
    Header,
    Loading
} from '../../../components';


class HomeClass extends React.Component {
    api = new API();

    constructor(props) {
        super(props);
        this.state = {
            loading: true,

            posts: [],
        };
        this.getPost()
    }

    getPost() {
        this.api.getPost()
            .then(data => {
                this.setState({
                    loading: false
                })
                if (data.status === 200) {
                    this.setState({
                        posts: data.data.posts
                    })
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err);
            })
    }

    _renderTitleImg(gallery) {
        let path
        gallery.some((data, index) => {
            if (data.type === 'img') {
                path = data.path
            }

        })
        return (
            <>
                <Image source={{uri: `${Constants.baseUrl}/${path}`}}
                       style={styles.post_item_img}/>
            </>
        )
    }


    _renderPost() {
        return this.state.posts.map((data, index) => {
            return (
                <TouchableOpacity style={styles.post_item_view} key={index} onPress={() => {
                    this.props.navigation.navigate('PostInfo', {post: data})
                }}>
                    <View style={styles.church_item_info}>
                        <Image source={{uri: `${Constants.baseUrl}/${data.church_photo}`}}
                               style={{width: 32, height: 32, borderRadius: 50, resizeMode: 'cover'}}/>
                        <Text style={styles.post_item_name_text}>{data.church_name}</Text>
                    </View>

                    <View style={{paddingBottom: 22, alignItems: 'center'}}>

                        {this._renderTitleImg(data.posts_gallery)}
                    </View>
                    <Text style={styles.post_item_description} numberOfLines={5}>
                        {data.description}
                    </Text>

                </TouchableOpacity>
            );
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <ScrollView>
                    <View>
                        <ImageBackground source={require('../../../assets/img/post_img.png')}
                                         style={styles.post_title_img}>
                            <Text style={styles.post_title}>To be in the world and not be affected by the world</Text>
                        </ImageBackground>
                    </View>
                    <View style={styles.post_view}>
                        <Text style={styles.post_text}> POST</Text>
                        <View style={{paddingBottom: 50}}>
                            {this._renderPost()}
                        </View>
                    </View>
                </ScrollView>
                <Loading loading={this.state.loading} />
            </View>
        );
    }
}

export const Home = connect(({customer}) => ({customer}))(HomeClass);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom: 50,
    },
    post_title_img: {
        paddingTop: 51,
        width: '100%',
        height: 360,
        alignItems: 'center',
    },
    post_title: {
        color: 'white',
        textShadowColor: '#ffa41b',
        textShadowOffset: {width: 3, height: 2},
        textShadowRadius: 2,
        fontSize: 31,
        textAlign: 'center',
        marginHorizontal: '10%',
        lineHeight: 41,
        fontFamily: 'SegoeUI',
    },
    post_view: {
        backgroundColor: '#fff',
        paddingHorizontal: 16,
    },
    post_text: {
        marginTop: 12,
        color: '#515151',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI-Bold',
    },
    post_item_view: {
        marginTop: 20,
        paddingBottom: 20,
        borderRadius: 5,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    church_item_info: {
        paddingLeft: 16,
        paddingTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    post_item_name_text: {
        marginLeft: 10,
        fontSize: 14,
        lineHeight: 19,
        color: '#000000',
        fontFamily: 'SegoeUI-Bold',
    },
    post_item_img: {
        borderWidth: 1,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        marginTop: 17,
        width: '100%',
        height: 232,
    },
    join_btn: {
        position: 'absolute',
        bottom: 6,
        backgroundColor: '#FFD500',
        width: 72,
        height: 29,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
    },
    join_text: {
        color: '#000000',
        fontSize: 14,
        lineHeight: 19,
        fontFamily: 'SegoeUI-Bold',
    },
    reviews_view: {
        position: 'absolute',
        top: 20,
        width: '100%',
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    post_item_description: {
        paddingHorizontal: 16,
        color: '#515151',
        fontSize: 14,
        lineHeight: 19,
    },
    line_text: {
        height: 0,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        marginHorizontal: 16,
        marginVertical: 12,
    },
    btn_view: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
    },
    shaer_like_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    review_count_view: {
        borderWidth: 1,
        borderColor: '#fff',
        width: 30,
        height: 30,
        borderRadius: 50,
        backgroundColor: '#FFF300',
        alignItems: 'center',
        justifyContent: 'center'
    },
    review_count_text: {
        color: '#000000',
        fontSize: 12,
        lineHeight: 16,
        fontFamily: 'SegoeUI-Bold',
    },
});


