import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text, TouchableOpacity
} from 'react-native';
import {
    Header,
    Video,
} from '../../../components';
import {connect} from "react-redux";
import {Constants} from "../../../constants";

class PostInfoClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            post: props.route.params.post,
            play: {},
            activeVideo: null
        };
        this.videoPlay();
    }

    videoPlay(){
        for(let i = 0; i < this.props.route.params.post.posts_gallery.length; i++){
            if(this.props.route.params.post.posts_gallery[i].type === 'video'){
                let obj = this.state.play
                obj[i] = true
                this.setState({
                    play: obj
                })
            }
        }
    }

    _renderTitleImg() {
        let path
        this.state.post.posts_gallery.some((data, index) => {
            if (data.type === 'img') {
                path = data.path
            }

        })
        return (
            <>
                <Image source={{uri: `${Constants.baseUrl}/${path}`}}
                       style={{width: '100%', height: 405}}/>
            </>
        )
    }

    play = (index) => {
        let obj = this.state.play
        if(this.state.activeVideo !== null &&  this.state.activeVideo !== index){
            obj[this.state.activeVideo] = true
        }
        obj[index] = !this.state.play[index]
        this.setState({
            activeVideo: index,
            play: obj
        })
    }

    _renderGallery(gallery){
        return gallery.map((data, index) => {
            if (data.type === 'video') {
                return (
                    <View style={{width: 100, height: 100, marginTop: 10, marginRight: 10}}>
                        <Video uri={data.path} key={index} paused={this.state.play[index]} index={index} play={this.play}/>
                    </View>
                )
            }else{
                return <Image source={{uri: `${Constants.baseUrl}/${data.path}`}} style={{width: 100, height: 100, marginTop: 10, marginRight: 10}}
                              key={index}/>
            }
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Header back={true} navigation={this.props.navigation}/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    {this._renderTitleImg()}
                    <View style={styles.post_info}>
                        <View style={styles.church_info}>
                            <Image source={{uri: `${Constants.baseUrl}/${this.state.post.church_photo}`}}
                                   style={{width: 32, height: 32, borderRadius: 50, resizeMode: 'cover'}}/>
                            <Text style={styles.post_name_text}>{this.state.post.church_name}</Text>
                        </View>
                        <Text style={styles.post_description}>
                            {this.state.post.description}
                        </Text>
                        <View style={styles.gallary_view}>
                            {this._renderGallery(this.state.post.posts_gallery)}
                        </View>
                        {this.props.customer.role === 'church' ?
                            <View style={styles.icon_view}>
                                <TouchableOpacity>
                                    <Image source={require('../../../assets/img/edit.png')}
                                           style={{width: 20, height: 20, resizeMode: 'contain'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image source={require('../../../assets/img/trash.png')}
                                           style={{width: 20, height: 20, resizeMode: 'contain'}}/>
                                </TouchableOpacity>
                            </View>
                            :
                            null
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
    componentWillUnmount() {
        let obj = this.state.play;
        obj[this.state.activeVideo] = true
        this.setState({
            activeVideo: null,
            play: obj
        })
    }
}

export const PostInfo = connect(({config, customer}) => ({config, customer}))(PostInfoClass);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    post_info: {
        marginBottom: 80,
        paddingBottom: 10,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        width: '90%',
        marginTop: -80,
        marginHorizontal: '5%',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    church_info: {
        paddingLeft: 16,
        paddingTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    post_name_text: {
        marginLeft: 10,
        fontSize: 14,
        lineHeight: 19,
        color: '#000000',
        fontFamily: 'SegoeUI-Bold',
    },
    post_description: {
        marginTop: 8,
        textAlign: 'center',
        paddingHorizontal: 16,
        color: '#515151',
        fontSize: 14,
        lineHeight: 19,
    },
    gallary_view: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    reviews_view: {
        width: '100%',
        marginTop: 27,
        flexDirection: 'row',
        paddingLeft: 16,
        justifyContent: 'flex-start',
    },
    review_count_view: {
        borderWidth: 1,
        borderColor: '#fff',
        width: 30,
        height: 30,
        borderRadius: 50,
        backgroundColor: '#FFF300',
        alignItems: 'center',
        justifyContent: 'center'
    },
    review_count_text: {
        color: '#000000',
        fontSize: 12,
        lineHeight: 16,
        fontFamily: 'SegoeUI-Bold',
    },
    line_text: {
        height: 0,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        marginHorizontal: 16,
        marginVertical: 12,
    },
    btn_view: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
    },
    shaer_like_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 11
    },
});


