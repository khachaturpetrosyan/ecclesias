import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux'
import { connectSocket } from '../../../networking/socket';
import {Constants} from "../../../constants";


class HelloClass extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.hello}>
                <StatusBar backgroundColor="white" barStyle='dark-content'/>
                <ScrollView contentContainerStyle={styles.scrollContainer}>

                    <Text style={styles.mainTitle}>the ecclesias</Text>
                    <Text style={styles.infoText}>To be in the world and not be affected by the world </Text>
                    <View style={styles.userContainer}>
                        <Text style={styles.userText}>Welcome back</Text>
                        <TouchableOpacity onPress={() => {
                            if (this.props.customer.role === 'church') {
                                this.props.navigation.navigate('TabChurchNavigator')
                            } else {
                                this.props.navigation.navigate('TabNavigator')
                            }
                        }}>
                            {this.props.customer.role === 'church' ?
                                <Image
                                    source={this.props.customer.photo ? {uri: `${Constants.baseUrl}/${this.props.customer.photo}`} : require('../../../assets/img/churchAvatar.png')}
                                    style={styles.userImage}/>
                                :
                                <Image
                                    source={this.props.customer.photo ? {uri: `${Constants.baseUrl}/${this.props.customer.photo}`} : require('../../../assets/img/user1.png')}
                                    style={styles.userImage}/>
                            }
                        </TouchableOpacity>
                        <Text style={styles.userText}>Matt</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
    componentDidMount() {
        connectSocket(this.props.customer)
    }
}

export const Hello = connect(({customer}) => ({customer}))(HelloClass)

const styles = StyleSheet.create({
    hello: {
        flex: 1,
        backgroundColor: '#fff'
    },
    scrollContainer: {
        alignItems: 'center',
        paddingBottom: 20
    },
    header: {
        height: 51,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        width: '100%',
    },
    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    arrowLeft: {
        width: 24,
        height: 24,
    },
    back: {
        fontSize: 12,
        color: '#000',
        fontFamily: 'SegoeUI',
        lineHeight: 16,
        paddingLeft: 10,
    },
    loginContainer: {},
    loginText: {
        fontSize: 20,
        color: '#000',
        fontFamily: 'SegoeUI-Bold',
        lineHeight: 27,
    },
    mainTitle: {
        fontSize: 42,
        color: '#FFD500',
        fontFamily: 'SegoeUI-Bold',
        lineHeight: 66,
        paddingTop: 109,
        paddingBottom: 17,
        textTransform: 'capitalize'
    },
    infoText: {
        fontSize: 12,
        color: '#9F9F9F',
        fontFamily: 'regular',
        lineHeight: 16,
        textAlign: 'center',
        width: 206
    },
    userContainer: {
        paddingTop: 139
    },
    userText: {
        fontSize: 14,
        color: '#000',
        fontFamily: 'SegoeUI-Bold',
        lineHeight: 19,
        textAlign: 'center'
    },
    userImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginTop: 29,
        marginBottom: 13
    },
});


// fontFamily

//   'segoeuib' bold
//   'segoeuil' light
//   'segoeui' regular
//   'seguisb' semiBold
//   'segoeuisl' semiLight
