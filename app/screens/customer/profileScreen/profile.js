import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    TextInput,
    Platform
} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'
import API from '../../../networking/api'
import {Constants} from '../../../constants'
import {ChooseChurch, Loading} from '../../../components'
import {socket} from "../../../networking/socket";


class ProfileClass extends Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            modallCountry: false,
            modallChurch: false,
            edit: false,
            loading: false,

            firstName: props.customer.first_name,
            lastName: props.customer.last_name,
            phone: props.customer.phone,
            country: props.customer.country,
            myChurch: props.customer.member_church_name,
            myChurchId: props.customer.member_church_id,
            email: props.customer.email,
            avatar: props.customer.photo ? {uri: `${Constants.baseUrl}/${props.customer.photo}`} : require('../../../assets/img/user1.png')
        }
    }

    removeValue = async () => {
        try {
            await AsyncStorage.removeItem('token')
            this.props.navigation.navigate('LoginNavigator')
        } catch (e) {
        }
    }

    handleChange = (e, name) => {
        this.setState({
            [name]: e,
        });
    };

    _rendercountry() {
        if (this.state.modallCountry) {
            return (
                <CountryPicker
                    countryCode={'AM'}
                    onSelect={(value) => {
                        this.setState({
                            country: value.name,
                        });
                    }}
                    translation='eng'
                    visible={true}
                    withCallingCode={true}
                    withFilter={true}
                    withFlag={true}
                    onClose={() => {
                        this.setState({modallCountry: false});
                    }}
                />
            );
        }
    }

    async checkAllPermissions() {
        try {
            if (Platform.OS == 'ios') {
                return true
            }
            const granted = await PermissionsAndroid.requestMultiple(
                [
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
                ],
            );
            if (
                granted['android.permission.CAMERA'] === PermissionsAndroid.RESULTS.GRANTED &&
                granted['android.permission.WRITE_EXTERNAL_STORAGE'] === PermissionsAndroid.RESULTS.GRANTED
            ) {
                return true
            } else {
                return false
            }
        } catch (err) {
            console.warn(err);
        }
    };

    selectPhoto() {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = {uri: response.uri};
                this.setState({
                    avatar: source
                })

            }
        });
    }

    submit() {
        this.setState({
            loading: true
        })
        let info = {
            photo: this.state.avatar.uri,
            lastName: this.state.lastName,
            firstName: this.state.firstName,
            phone: this.state.phone,
            email: this.state.email,
            country: this.state.country,
        }
        this.api.changeProfile(info)
            .then(customer => {
                this.setState({
                    loading: false
                })
                if (church.status === 200) {
                    this.props.dispatch({
                        type: 'SET_CUSTOMER', value: {
                            id: customer.data.id,
                            email: customer.data.email,
                            last_name: customer.data.last_name,
                            first_name: customer.data.first_name,
                            phone: customer.data.phone,
                            photo: customer.data.photo,
                            country: customer.data.country,
                        },
                    });
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err);
            })

    }

    render() {
        return (
            <View style={styles.content}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    <Image source={require('../../../assets/img/avatarBig.png')}
                           style={{width: '100%', height: 248, resizeMode: 'cover'}}/>
                    <View style={styles.body_view}>
                        <View style={styles.imgs_view}>
                            <TouchableOpacity disabled={!this.state.edit} onPress={() => this.selectPhoto()}>
                                <Image
                                    source={this.state.avatar}
                                    style={{
                                        width: 108,
                                        marginTop: -50,
                                        height: 108,
                                        borderRadius: 50,
                                        resizeMode: 'cover'
                                    }}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({edit: !this.state.edit})}>
                                <Image source={require('../../../assets/img/edit.png')}
                                       style={{width: 23, height: 23}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.form_view}>
                            <TextInput
                                editable={this.state.edit}
                                value={this.state.firstName}
                                placeholder="Email"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'firstName')}
                                style={[styles.input, this.state.edit ? {borderWidth: 1} : {borderWidth: 0}]}
                            />
                            <TextInput
                                editable={this.state.edit}
                                value={this.state.lastName}
                                placeholder="Last Name"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'lastName')}
                                style={[styles.input, this.state.edit ? {borderWidth: 1} : {borderWidth: 0}]}
                            />
                            <TextInput
                                editable={this.state.edit}
                                value={this.state.phone}
                                placeholder="Phone"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'phone')}
                                style={[styles.input, this.state.edit ? {borderWidth: 1} : {borderWidth: 0}]}
                            />
                            <TouchableOpacity
                                style={[styles.country_view, this.state.edit ? {borderWidth: 1} : {borderWidth: 0}]}
                                onPress={() => {
                                    if (this.state.edit) {
                                        this.setState({modallCountry: true})
                                    }
                                }}>
                                <TextInput
                                    editable={false}
                                    selectTextOnFocus={false}
                                    placeholder="Country"
                                    value={this.state.country}
                                    placeholderTextColor="#000"
                                    onChangeText={(e) => this.handleChange(e, 'country')}
                                    style={[styles.country_input,]}
                                />
                                {this.state.edit ?
                                    <View>
                                        <Image source={require('../../../assets/img/down.png')}
                                               style={{width: 20, resizeMode: 'contain', height: 20}}/>
                                    </View>
                                    :
                                    null
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.country_view, this.state.edit ? {borderWidth: 1} : {borderWidth: 0}]}
                                onPress={() => {
                                    if (this.state.edit) {
                                        this.setState({modallChurch: true})
                                    }
                                }}>
                                <TextInput
                                    editable={false}
                                    selectTextOnFocus={false}
                                    placeholder="My Church"
                                    value={this.state.myChurch}
                                    placeholderTextColor="#000"
                                    onChangeText={(e) => this.handleChange(e, 'myChurch')}
                                    style={[styles.country_input,]}
                                />
                                {this.state.edit ?
                                    <View>
                                        <Image source={require('../../../assets/img/down.png')}
                                               style={{width: 20, resizeMode: 'contain', height: 20}}/>
                                    </View>
                                    :
                                    null
                                }
                            </TouchableOpacity>
                            <TextInput
                                editable={this.state.edit}
                                value={this.state.email}
                                placeholder="Email"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'email')}
                                style={[styles.input, this.state.edit ? {borderWidth: 1} : {borderWidth: 0}]}
                            />
                            {this.state.edit ?
                                <TouchableOpacity style={styles.save_btn} onPress={() => this.submit()}>
                                    <Text style={styles.save_btn_text}>Save</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.log_out} onPress={() => {
                                    socket.emit('delete', {email: this.props.customer.email});
                                    this.removeValue()
                                }}>
                                    <Text style={styles.log_out_text}>Log out</Text>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                    {this._rendercountry()}
                    <ChooseChurch
                        close={(church_name, church_id,) => {
                            this.setState({
                                modallChurch: false,
                                myChurch: church_name,
                                myChurchId: church_id
                            });
                        }}
                        isVisible={this.state.modallChurch}
                    />
                </ScrollView>
                <Loading loading={this.state.loading}/>
            </View>
        );
    }
}

export const Profile = connect(({config, customer}) => ({config, customer}))(ProfileClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    body_view: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginTop: -100,
    },
    imgs_view: {
        marginHorizontal: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    form_view: {
        marginTop: 50,
        paddingHorizontal: 23,
    },
    input: {
        fontSize: 14,
        marginTop: 25,
        height: 40,
        paddingLeft: 10,
        color: '#000',
        borderWidth: 1,
        borderColor: '#9F9F9F',
        borderRadius: 15,
    },
    country_view: {
        marginTop: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        borderWidth: 1,
        paddingRight: 10,
        borderColor: '#9F9F9F',
        borderRadius: 15,
    },
    country_input: {
        width: '80%',
        height: 40,
        paddingLeft: 10,
        color: '#000',
    },
    save_btn: {
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        backgroundColor: '#fff300',
        height: 45,
        marginBottom: 75
    },
    save_btn_text: {
        color: '#000',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI',
    },
    log_out: {
        paddingLeft: 10,
        marginBottom: 80,
        marginTop: 20,
        height: 45,
    },
    log_out_text: {
        color: '#000',
        fontSize: 14,
        lineHeight: 27,
        fontFamily: 'SegoeUI',
    }
});
