import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    ImageBackground,
    TextInput,
} from 'react-native';
import {connect} from "react-redux";
import moment from 'moment';
import API from '../../../networking/api';
import {
    Header,
    Loading,
    CustomerChat
} from '../../../components';
import {Constants} from "../../../constants";


class ChatListClass extends React.Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            modallChustomerChat: false,

            data: [],
        };
        this.getCustomers()
    }

    getCustomers() {
        this.api.getChat()
            .then(chats => {
                this.setState({
                    loading: false
                })
                if (chats.status === 200) {
                    this.setState({
                        data: chats.data.chats
                    })
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
            })
    }

    _renderChatList() {
        return this.state.data.map((data, index) => {
            console.log(data.message[data.message.length - 1].status);
            return (
                <TouchableOpacity key={index} style={styles.chat_view} onPress={() => {
                    this.props.navigation.navigate('Chat', {data})
                }}>
                    <View style={styles.chat_name_view}>
                        {data.sender_id === this.props.customer.id ?
                            data.receiver_photo ?
                                <Image source={{uri: `${Constants.baseUrl}/${data.receiver_photo}`}}
                                       style={{width: 50, height: 50, borderRadius: 50}}/>
                                :
                                <Image source={require('../../../assets/img/user1.png')}
                                       style={{width: 50, height: 50, borderRadius: 50}}/>
                            :
                            data.sender_photo ?
                                <Image source={{uri: `${Constants.baseUrl}/${data.sender_photo}`}}
                                       style={{width: 50, height: 50, borderRadius: 50}}/>
                                :
                                <Image source={require('../../../assets/img/user1.png')}
                                       style={{width: 50, height: 50, borderRadius: 50}}/>
                        }
                        <View style={styles.name_view}>
                            <Text style={styles.name_text}>
                                {data.sender_id === this.props.customer.id ?
                                    data.receiver_name
                                    :
                                    data.sender_name
                                }
                            </Text>
                            {data.message.length ?
                                <Text style={styles.chat_info_name}></Text>
                                :
                                <Text style={styles.chat_info_name}>No correspondence</Text>
                            }
                        </View>
                    </View>
                    {data.message.length ?
                        <View style={styles.chat_status_view}>

                            <Text
                                style={styles.date_text}>{moment(data.message[data.message.length - 1].created_at).format('MMMM Do YYYY, h:mm:ss a')}</Text>
                            {
                                data.message[data.message.length - 1].status ?
                                    <Text style={styles.status_text}/>
                                    :
                                    null
                            }
                        </View>
                        :
                        null}
                </TouchableOpacity>
            );
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Header back={true} navigation={this.props.navigation}/>
                <View>
                    <Text style={styles.title_text}>Search</Text>
                    <TouchableOpacity style={styles.search} onPress={() => {
                        this.setState({
                            modallChustomerChat: true
                        })
                    }}>
                        <View

                            style={styles.search_input}
                        >
                            <Text>Search by user or places</Text>
                        </View>
                        <View>
                            <Image source={require('../../../assets/img/search_icon.png')}
                                   style={{width: 24, height: 24,}}/>
                        </View>
                    </TouchableOpacity>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <Text style={styles.title_text}>Caht</Text>
                        {this._renderChatList()}
                    </View>
                </ScrollView>
                <Loading loading={this.state.loading}/>
                <CustomerChat
                    close={() => {
                        this.setState({
                            modallChustomerChat: false,
                        });
                    }}
                    action={(data) => {
                        this.setState({
                            modallChustomerChat: false,
                        })
                        this.props.navigation.navigate('Chat', {data})
                    }}
                    isVisible={this.state.modallChustomerChat}
                />
            </View>
        );
    }
}

export const ChatList = connect(({customer}) => ({customer}))(ChatListClass);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom: 50,
    },
    title_text: {
        marginTop: 16,
        marginLeft: 25,
        color: '#000',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI-Bold',
    },
    search: {
        marginTop: 18,
        marginHorizontal: 16,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: '#DFDFDF',
        height: 41,
        borderRadius: 5,
    },
    search_input: {
        width: '80%',
    },
    chat_view: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
    },
    chat_name_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    name_view: {
        marginLeft: 17
    },
    name_text: {
        color: '#000000',
        fontSize: 12,
        lineHeight: 16,
        fontFamily: 'SegoeUI-Bold',
    },
    chat_info_name: {
        color: '#000000',
        fontSize: 12,
        lineHeight: 16,
        marginTop: 7
    },
    chat_status_view: {
        alignItems: 'flex-end'
    },
    date_text: {
        color: '#9F9F9F',
        fontSize: 12,
        lineHeight: 16
    },
    status_text: {
        marginTop: 10,
        width: 10,
        height: 10,
        borderRadius: 50,
        backgroundColor: '#FFD500'
    },
});


