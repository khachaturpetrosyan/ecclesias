import React from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import {GiftedChat, Bubble} from 'react-native-gifted-chat';
import {connect} from "react-redux";
import API from '../../../networking/api';
import {socket} from "../../../networking/socket";

class ChatClass extends React.Component {
    api = new API()
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            loadEarlier: true,
            typingText: null,
            isLoadingEarlier: false,
        };

        this._isMounted = false;
        this.onSend = this.onSend.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.onLoadEarlier = this.onLoadEarlier.bind(this);

        this._isAlright = null;
        this.mess()
    }

    async mess(){
        let arr = []
        await  this.props.route.params.data.message.map(value => {
            arr.unshift(value.message)
        })
        await this.setState({
            messages: arr
        })
    }

    componentDidMount() {
        this._isMounted = true;
        socket.on('new_message', data => {
            this.setState((previousState) => {
                return {
                    messages: GiftedChat.append(previousState.messages, data.message),
                };
            });
        })
        this.api.readChat(this.props.route.params.data.id)
            .then(data => {
                if(data.status === 200){
                    this.props.dispatch({
                        type: 'SET_CUSTOMER', value: {
                            new_chat: null,
                        },
                    });
                }
            })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onLoadEarlier() {
        this.setState((previousState) => {
            return {
                isLoadingEarlier: true,
            };
        });

        setTimeout(() => {
            if (this._isMounted === true) {
                this.setState((previousState) => {
                    return {
                        messages: GiftedChat.prepend(previousState.messages,[]),
                        loadEarlier: false,
                        isLoadingEarlier: false,
                    };
                });
            }
        }, 1000); // simulating network
    }

    onSend(messages = []) {
        messages[0].user.avatar = this.props.customer.photo
        messages[0].user.name =  `${this.props.customer.last_name} ${this.props.customer.first_name}`
        messages[0].user.email = this.props.customer.email
        socket.emit('send_message', {
            message: messages[0],
            receiver_email:this.props.customer.email === this.props.route.params.data.receiver_email ? this.props.route.params.data.sender_email : this.props.route.params.data.receiver_email,
            receiver_photo: this.props.route.params.data.photo,
            receiver_id:  this.props.customer.id === this.props.route.params.data.receiver_id ? this.props.route.params.data.sender_id : this.props.route.params.data.receiver_id,
            receiver_name: this.props.route.params.data.receiver_name ? this.props.route.params.data.receiver_name : `${this.props.route.params.data.first_name} ${this.props.route.params.data.last_name}`,
        })
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, messages),
            };
        });

        // for demo purpose
        this.answerDemo(messages);
    }

    answerDemo(messages) {
        if (messages.length > 0) {
            if ((messages[0].image || messages[0].location) || !this._isAlright) {
                this.setState((previousState) => {
                    return {
                        typingText: 'React Native is typing'
                    };
                });
            }
        }

        setTimeout(() => {

            this.setState((previousState) => {
                return {
                    typingText: null,
                };
            });
        }, 1000);
    }

    renderBubble(props) {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    left: {
                        backgroundColor: '#f0f0f0',
                    }
                }}
            />
        );
    }


    renderFooter(props) {
        if (this.state.typingText) {
            return (
                <View style={styles.footerContainer}>
                    <Text style={styles.footerText}>
                        {this.state.typingText}
                    </Text>
                </View>
            );
        }
        return null;
    }

    render() {
        return (
            <GiftedChat
                messages={this.state.messages}
                onSend={this.onSend}
                loadEarlier={this.state.loadEarlier}
                onLoadEarlier={this.onLoadEarlier}
                isLoadingEarlier={this.state.isLoadingEarlier}

                user={{
                    _id: this.props.customer.id, // sent messages should have same user._id
                }}

                renderBubble={this.renderBubble}
                renderFooter={this.renderFooter}
            />
        );
    }
}

export const Chat = connect(({customer}) => ({customer}))(ChatClass);


const styles = StyleSheet.create({
    footerContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    footerText: {
        fontSize: 14,
        color: '#aaa',
    },
});