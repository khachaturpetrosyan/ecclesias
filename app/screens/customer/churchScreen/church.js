import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import API from '../../../networking/api'
import {
    Header,
    Loading
} from '../../../components';
import {Constants} from "../../../constants";


class ChurchClass extends React.Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            loading: true,

            invitation_churche: [],
            churches: []
        };
        this.getInvitationChurches()
    }

    getInvitationChurches() {
        this.setState({
            loading: true
        })
        this.api.getInvitationChurches()
            .then(churches => {
                this.setState({
                    loading: false
                })
                if (churches.status === 200) {
                    this.setState({
                        invitation_churche: churches.data.invitation_churche,
                        churches: churches.data.churches,
                    })
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err, 'err');
            })
    }

    invitation(id) {
        this.setState({
            loading: true
        })
        this.api.invitationChurches(id)
            .then(data => {
                if (data.status === 200) {
                    return this.getInvitationChurches()
                }
                this.setState({
                    loading: false
                })
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err);
            })
    }

    _renderChurchesList() {
        return this.state.churches.map((data, index) => {
            return (
                <TouchableOpacity style={styles.church_view} onPress={() => {
                    this.invitation(data.id)
                }}>
                    <View style={styles.church_item_view}>
                        {data.photo ?
                            <Image source={{uri: `${Constants.baseUrl}/${data.photo}`}}
                                   style={{width: 50, height: 50, borderRadius: 50, resizeMode: 'contain'}}/>
                            :
                            <Image source={require('../../../assets/img/churchAvatar.png')}
                                   style={{width: 50, height: 50}}/>
                        }
                        <Text style={styles.church_name}>{data.name}</Text>
                    </View>
                    <TouchableOpacity style={styles.follow_btn}>
                        <Text style={styles.follow_btn_text}>Follow</Text>
                    </TouchableOpacity>
                </TouchableOpacity>
            )
        })
    }

    invitation_remove(id) {
        this.setState({
            loading: true
        })
        this.api.invitationRemoveChurches(id)
            .then(data => {
                if (data.status === 200) {
                    return this.getInvitationChurches()
                }
                this.setState({
                    loading: false
                })
            })
            .catch(err => {
                this.setState({
                    loading: true
                })
            })
    }

    _renderChurchesInvitationList() {
        return this.state.invitation_churche.map((data, index) => {
            return (
                <TouchableOpacity style={styles.church_view} onPress={() => {
                    this.invitation_remove(data.id)
                }}>
                    <View style={styles.church_item_view}>
                        {data.photo ?
                            <Image source={{uri: `${Constants.baseUrl}/${data.church.photo}`}}
                                   style={{width: 50, height: 50, borderRadius: 50, resizeMode: 'contain'}}/>
                            :
                            <Image source={require('../../../assets/img/churchAvatar.png')}
                                   style={{width: 50, height: 50}}/>
                        }
                        <Text style={styles.church_name}>{data.church.name}</Text>
                    </View>
                    <Image source={require('../../../assets/img/close.png')}
                           style={{width: 20, resizeMode: 'contain', height: 20}}/>
                </TouchableOpacity>
            )
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Header back={true} navigation={this.props.navigation}/>
                <View>
                    <Text style={styles.title_text}>Church</Text>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{marginTop: 38,}}>
                        {this._renderChurchesInvitationList()}
                        {this._renderChurchesList()}
                    </View>
                </ScrollView>
                <Loading loading={this.state.loading}/>
            </View>
        );
    }
}

export const Church = ChurchClass;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom: 50,
    },
    title_text: {
        marginTop: 16,
        marginLeft: 25,
        color: '#000',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI-Bold',
    },
    church_view: {
        height: 70,
        borderBottomColor: '#9F9F9F',
        borderBottomWidth: 0.5,
        marginHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    church_item_view: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    church_name: {
        color: '#000000',
        fontSize: 13,
        fontWeight: 'bold',
        lineHeight: 17,
        marginLeft: 17,
    },
    follow_btn: {
        width: 119,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFF300',
        borderRadius: 50
    },
    follow_btn_text: {
        color: '#000000',
        fontSize: 12
    }
});


