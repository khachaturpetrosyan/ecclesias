import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    TextInput,
    Dimensions,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {Header} from '../../../components/header';


const windowWidth = Dimensions.get('window').width;


class LocationClass extends Component {
    constructor() {
        super();
        this.state = {
            showInput: false,
        };
    }


    render() {
        return (

            <View style={styles.container}>
                <Header back={true} navigation={this.props.navigation}/>
                <MapView
                    style={{width: '100%', height: '100%'}}
                    mapType={'terrain'}
                >
                </MapView>
                {this.state.showInput &&
                <View style={styles.search_view}>
                    <Image source={require('../../../assets/img/location-yellow.png')}
                           style={{width: 18, height: 18, resizeMode: 'contain'}}/>
                    <TextInput
                        style={styles.input}
                        placeholder={'Search Church'}
                    />
                </View>
                }
                <TouchableOpacity style={styles.search_btn} onPress={() => this.setState({showInput: !this.state.showInput})}>
                    <Image source={require('../../../assets/img/search_icon.png')} style={{
                        width: 20, height: 20,
                        tintColor: '#fff', resizeMethod: 'contain',
                    }}/>
                </TouchableOpacity>
            </View>
        );
    }
}

export const Location = LocationClass;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    close_btn: {
        width: 60,
        height: 60,
        position: 'absolute',
        top: 16,
        left: 16,
    },
    search_btn: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        backgroundColor: '#37ADDB',
        width: 54,
        height: 54,
        position: 'absolute',
        bottom: 76,
        right: windowWidth / 2 - 27,
    },
    search_view: {
        position: 'absolute',
        top: 50,
        height: 41,
        alignItems: 'center',
        paddingLeft: 10,
        marginTop: 25,
        flexDirection: 'row',
        paddingRight: 10,
        borderColor: '#9F9F9F',
        width: '90%',
        marginHorizontal: '10%',


        borderRadius: 12,
        backgroundColor: '#fff',
        overflow: 'hidden',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    input: {
        fontSize: 12,
        paddingLeft: 10,

    },
});
