import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text, TouchableOpacity, TextInput
} from 'react-native';
import {
    Header,
    Loading,
    Video,
} from '../../../components';
import API from '../../../networking/api';
import {connect} from "react-redux";
import {Constants} from "../../../constants";
import ImagePicker from "react-native-image-picker";

class EditPostClass extends React.Component {
    api = new API()
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            err: false,

            activeVideo: null,
            play: {},
            delete_id: [],
            description: props.route.params.post.description,
            posts_gallery: props.route.params.post.posts_gallery,
            post: props.route.params.post,


        };
        this.videoPlay();
    }

    handleChange = (e, name) => {
        this.setState({
            [name]: e,
            err: false,
        });
    };

    edit(){
        if (this.state.description) {
            let data = {
                post_id: this.props.route.params.post.id,
                description: this.state.description,
                gallary: this.state.posts_gallery,
                delete_id: this.state.delete_id
            }
            this.setState({
                loading: true
            })
            this.api.editPost(data)
                .then(data => {
                    this.setState({
                        loading: false
                    })
                      if(data.status === 200) {
                          this.setState({
                              play: {},
                              delete_id: [],
                          })
                          this.props.navigation.navigate('HomeChurch')
                      }
                })
                .catch(err => {
                    this.setState({
                        loading: false
                    })
                    console.log(err);
                })
        } else {
            this.setState({
                err: true
            })
        }
    }

    videoPlay() {
        for (let i = 0; i < this.props.route.params.post.posts_gallery.length; i++) {
            if (this.props.route.params.post.posts_gallery[i].type === 'video') {
                let obj = this.state.play
                obj[i] = true
                this.setState({
                    play: obj
                })
            }
        }
    }

    _renderTitleImg() {
        let path
        this.state.post.posts_gallery.some((data, index) => {
            if (data.type === 'img') {
                path = data.path
            }

        })
        return (
            <>
                <Image source={{uri: `${Constants.baseUrl}/${path}`}}
                       style={{width: '100%', height: 405}}/>
            </>
        )
    }

    play = (index) => {
        let obj = this.state.play
        if (this.state.activeVideo !== null && this.state.activeVideo !== index) {
            obj[this.state.activeVideo] = true
        }
        obj[index] = !this.state.play[index]
        this.setState({
            activeVideo: index,
            play: obj
        })
    }

    selectPhoto() {
        const options = {
            title: 'Select Poto',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let arr = this.state.posts_gallery
                const source = {path: response.uri, type: 'photo', local: true};
                arr.push(source)
                this.setState({
                    posts_gallery: arr
                })
            }
        });
    }

    selectVideo() {
        const options = {
            mediaType: 'video',
            title: 'Select Video',
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let arr = this.state.posts_gallery
                const source = {path: response.uri, type: 'video',  local: true};
                arr.push(source)
                this.setState({
                    posts_gallery: arr
                })

            }
        });
    }

    _renderGallery(gallery) {
        return gallery.map((data, index) => {
            if (data.type === 'video') {
                return (
                    <View style={{width: 100, height: 100, marginTop: 20, marginRight: 10}}>
                        <Video uri={data.path} key={index} paused={this.state.play[index]} index={index}
                               play={this.play} edit={true}/>
                        <TouchableOpacity style={styles.delete_btn} onPress={() => {
                            this.deleteimgVideo(index, data)
                        }}>
                            <Image source={require('../../../assets/img/close.png')}
                                   style={{width: 15, height: 15, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                    </View>
                )
            } else {
                return (
                    <View style={{width: 100, height: 100, marginTop: 20, marginRight: 10}}>
                        {data.local ?
                            <Image source={{uri: `${data.path}`}}
                                   style={{width: 100, height: 100,}}
                                   key={index}/>
                            :
                            <Image source={{uri: `${Constants.baseUrl}/${data.path}`}}
                                   style={{width: 100, height: 100,}}
                                   key={index}/>
                        }
                        <TouchableOpacity style={styles.delete_btn} onPress={() => {
                            this.deleteimgVideo(index, data)
                        }}>
                            <Image source={require('../../../assets/img/close.png')}
                                   style={{width: 15, height: 15, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                    </View>)

            }
        })
    }

    deleteimgVideo(index, data) {
        let gallery = this.state.posts_gallery;
        let delete_id = this.state.delete_id
        if(data.id){
            delete_id.push({id: data.id, path: data.path})
        }
        gallery.splice(index, 1)
        this.setState({
            posts_gallery: gallery,
            delete_id: delete_id
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Header back={true} navigation={this.props.navigation}/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    <View style={styles.post_info}>
                        <View style={styles.church_info}>
                            <Image source={{uri: `${Constants.baseUrl}/${this.state.post.church_photo}`}}
                                   style={{width: 32, height: 32, borderRadius: 50, resizeMode: 'cover'}}/>
                            <Text style={styles.post_name_text}>{this.state.post.church_name}</Text>
                        </View>
                        <View style={styles.textAreaContainer}>
                            <TextInput
                                value={this.state.description}
                                style={styles.textArea}
                                onChangeText={(e) => this.handleChange(e, 'description')}
                                underlineColorAndroid="transparent"
                                placeholder="Type something"
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                multiline={true}
                            />
                        </View>
                        <TouchableOpacity style={styles.video_view} onPress={() => this.selectPhoto()}>
                            <Image source={require('../../../assets/img/video.png')} style={{width: 23, height: 23}}/>
                            <Text style={styles.video_text}>Photo</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.video_view} onPress={() => this.selectVideo()}>
                            <Image source={require('../../../assets/img/video.png')} style={{width: 23, height: 23}}/>
                            <Text style={styles.video_text}>Video</Text>
                        </TouchableOpacity>
                        <View style={styles.gallary_view}>
                            {this._renderGallery(this.state.posts_gallery)}
                        </View>
                        <View style={{height: 20}}>
                            {this.state.err ?
                                <Text style={styles.err_text}>Please enter description</Text>
                                :
                                null
                            }
                        </View>
                        {this.props.customer.role === 'church' ?
                            <View style={styles.icon_view}>
                                <TouchableOpacity style={styles.btn} onPress={() => this.edit()}>
                                    <Text style={styles.btn_text}>Edit</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.btn, {backgroundColor: '#D5D5D5'}]}>
                                    <Text style={styles.btn_text}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                            :
                            null
                        }
                    </View>
                </ScrollView>
                <Loading loading={this.state.loading}/>
            </View>
        );
    }

    componentWillUnmount() {
        let obj = this.state.play;
        obj[this.state.activeVideo] = true
        this.setState({
            activeVideo: null,
            delete_id: [],
            play: obj
        })
    }
}

export const EditPost = connect(({config, customer}) => ({config, customer}))(EditPostClass);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    post_info: {
        marginBottom: 80,
        paddingBottom: 10,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        width: '90%',
        marginTop: 20,
        marginHorizontal: '5%',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    church_info: {
        paddingLeft: 16,
        paddingTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    post_name_text: {
        marginLeft: 10,
        fontSize: 14,
        lineHeight: 19,
        color: '#000000',
        fontFamily: 'SegoeUI-Bold',
    },
    textAreaContainer: {
        marginTop: 20,
        marginHorizontal: 20,
        borderColor: '#EFEFEF',
        borderWidth: 1,
        padding: 5
    },
    textArea: {
        textAlignVertical: 'top',
        height: 350,
        justifyContent: "flex-start"
    },
    video_view: {
        marginTop: 20,
        marginLeft: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    video_text: {
        marginLeft: 10
    },
    post_description: {
        marginTop: 8,
        textAlign: 'center',
        paddingHorizontal: 16,
        color: '#515151',
        fontSize: 14,
        lineHeight: 19,
    },
    gallary_view: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    reviews_view: {
        width: '100%',
        marginTop: 27,
        flexDirection: 'row',
        paddingLeft: 16,
        justifyContent: 'flex-start',
    },
    review_count_view: {
        borderWidth: 1,
        borderColor: '#fff',
        width: 30,
        height: 30,
        borderRadius: 50,
        backgroundColor: '#FFF300',
        alignItems: 'center',
        justifyContent: 'center'
    },
    review_count_text: {
        color: '#000000',
        fontSize: 12,
        lineHeight: 16,
        fontFamily: 'SegoeUI-Bold',
    },
    line_text: {
        height: 0,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        marginHorizontal: 16,
        marginVertical: 12,
    },
    btn_view: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
    },
    shaer_like_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 11
    },
    err_text: {
        marginTop: 5,
        marginLeft: 20,
        color: 'red',
        fontSize: 14
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        height: 45,
        backgroundColor: '#37ADDB',
        borderRadius: 10,
    },
    btn_text: {
        fontSize: 20,
        lineHeight: 27,
        color: '#fff'
    },
    delete_btn: {
        backgroundColor: '#fff',
        borderRadius: 50,
        justifyContent: 'center',
        width: 25,
        height: 25,
        alignItems: 'center',
        position: 'absolute',
        right: -10,
        top: -5
    }
});


