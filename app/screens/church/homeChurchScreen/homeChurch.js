import React from 'react';
import {
    StyleSheet,
    Image,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {connect} from "react-redux";
import API from '../../../networking/api';
import {
    Header,
    Followers,
    Members,
    CreatePost,
    Loading
} from '../../../components';
import {Constants} from "../../../constants";


class HomeChurchClass extends React.Component {
    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            modallFollowers: false,
            modallMemebers: false,
            modallCreatePost: false,
            loading: false,

            post: null,
            avatar: props.customer.photo ? {uri: `${Constants.baseUrl}/${props.customer.photo}`} : require('../../../assets/img/churchAvatar.png'),
            posts: [],
        };
        this.getPost()
    }

    getPost() {
        this.setState({
            loading: true
        })
        this.api.getChurchPost()
            .then(data => {
                this.setState({
                    loading: false
                })
                if (data.status === 200) {
                    this.setState({
                        posts: data.data.posts
                    })
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err);
            })
    }

    deletePost(id) {
        this.setState({
            loading: true
        })
        this.api.deletePost(id)
            .then(data => {
                this.setState({
                    loading: false
                })
                if (data.status === 200) {
                    this.getPost()
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
                console.log(err);
            })
    }

    _renderPost() {
        return this.state.posts.map((data, index) => {
            return (
                <View style={styles.post_item_view} key={index}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('PostInfo', {post: data})}>
                        <View style={styles.church_item_info}>
                            {data.church_img ?
                                <Image source={{uri: `${Constants.baseUrl}/${data.church_img}`}}
                                       style={{width: 32, height: 32}}/>
                                :
                                <Image source={require('../../../assets/img/churchAvatar.png')}
                                       style={{width: 32, height: 32}}/>
                            }
                            <Text style={styles.post_item_name_text}>{data.church_name}</Text>
                        </View>

                        <View style={{paddingBottom: 22, alignItems: 'center'}}>
                            {this._renderTitleImg(data.posts_gallery)}
                        </View>
                        <Text style={styles.post_item_description} numberOfLines={5}>
                            {data.description}
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.icon_view}>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('EditPost', {post: data})
                        }}>
                            <Image source={require('../../../assets/img/edit.png')}
                                   style={{width: 20, height: 20, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.deletePost(data.id)}>
                            <Image source={require('../../../assets/img/trash.png')}
                                   style={{width: 20, height: 20, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        });
    }

    _renderTitleImg(gallery) {
        let path
        gallery.some((data, index) => {
            if (data.type === 'img') {
                path = data.path
            }

        })
        return (
            <>
                <Image source={{uri: `${Constants.baseUrl}/${path}`}}
                       style={styles.post_item_img}/>
            </>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <ScrollView>
                    <View style={styles.btn_view}>
                        <TouchableOpacity style={styles.btn} onPress={() => {
                            this.setState({
                                modallMemebers: true
                            })
                        }}>
                            <Text style={styles.btn_text}>Members</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn, {marginLeft: 30}]} onPress={() => {
                            this.setState({
                                modallFollowers: true
                            })
                        }}>
                            <Text style={styles.btn_text}>Invitations</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <ImageBackground source={require('../../../assets/img/post_img.png')}
                                         style={styles.post_title_img}>
                            <Text style={styles.post_title}>To be in the world and not be affected by the world</Text>
                        </ImageBackground>
                    </View>
                    <View style={styles.church_info_view}>
                        <Image source={this.state.avatar} style={styles.church_img}/>
                        <Text style={styles.church_name_text}>{this.props.customer.church_name}</Text>
                    </View>
                    <View style={styles.btn_create_view}>
                        <TouchableOpacity style={styles.create_btn} onPress={() => {
                            this.setState({
                                modallCreatePost: true
                            })
                        }}>
                            <Text style={styles.create_text}>Cteare Post</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.post_view}>
                        <Text style={styles.post_text}> POST</Text>
                        <View style={{paddingBottom: 50}}>
                            {this._renderPost()}
                        </View>
                    </View>
                </ScrollView>
                <Followers
                    close={() => {
                        this.setState({
                            modallFollowers: false,
                        });
                    }}
                    isVisible={this.state.modallFollowers}
                />
                <Members
                    close={() => {
                        this.setState({
                            modallMemebers: false,
                        });
                    }}
                    isVisible={this.state.modallMemebers}
                />
                <CreatePost
                    close={() => {
                        this.setState({
                            modallCreatePost: false,
                        });
                    }}
                    post={this.state.post}
                    isVisible={this.state.modallCreatePost}
                />
                <Loading loading={this.state.loading}/>
            </View>
        );
    }
    componentDidMount() {
        this.focused = this.props.navigation.addListener('focus', () => {
            this.getPost()
        });
    }

    componentWillUnmount() {
        this.focused()
    }
}

export const HomeChurch = connect(({customer}) => ({customer}))(HomeChurchClass);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom: 50,
    },
    btn_view: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10
    },
    btn: {
        width: 119,
        height: 30,
        borderRadius: 50,
        borderColor: '#FFF300',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btn_text: {
        color: '#000'
    },
    post_title_img: {
        paddingTop: 51,
        width: '100%',
        height: 360,
        alignItems: 'center',
    },
    post_title: {
        color: 'white',
        textShadowColor: '#ffa41b',
        textShadowOffset: {width: 3, height: 2},
        textShadowRadius: 2,
        fontSize: 31,
        textAlign: 'center',
        marginHorizontal: '10%',
        lineHeight: 41,
        fontFamily: 'SegoeUI',
    },
    church_info_view: {
        width: '100%',
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    church_img: {
        width: 150,
        height: 150,
        borderRadius: 75,
    },
    church_name_text: {
        marginTop: 15,
        fontSize: 20,
        lineHeight: 26,
        fontWeight: '700',
        color: '#000000'
    },
    btn_create_view: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    create_btn: {
        marginTop: 10,
        width: 127,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#37ADDB',
        borderRadius: 10
    },
    create_text: {
        color: '#fff',
        fontSize: 14,
        lineHeight: 19
    },
    post_view: {
        backgroundColor: '#fff',
        paddingHorizontal: 16,
    },
    post_text: {
        marginTop: 12,
        color: '#515151',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI-Bold',
    },
    post_item_view: {
        marginTop: 20,
        paddingBottom: 20,
        borderRadius: 5,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    church_item_info: {
        paddingLeft: 16,
        paddingTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    post_item_name_text: {
        marginLeft: 10,
        fontSize: 14,
        lineHeight: 19,
        color: '#000000',
        fontFamily: 'SegoeUI-Bold',
    },
    post_item_img: {
        borderWidth: 1,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        marginTop: 17,
        width: '100%',
        height: 232,
    },
    join_btn: {
        position: 'absolute',
        bottom: 6,
        backgroundColor: '#FFD500',
        width: 72,
        height: 29,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
    },
    join_text: {
        color: '#000000',
        fontSize: 14,
        lineHeight: 19,
        fontFamily: 'SegoeUI-Bold',
    },
    reviews_view: {
        position: 'absolute',
        top: 20,
        width: '100%',
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    post_item_description: {
        paddingHorizontal: 16,
        color: '#515151',
        fontSize: 14,
        lineHeight: 19,
    },
    icon_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 11
    },
});


