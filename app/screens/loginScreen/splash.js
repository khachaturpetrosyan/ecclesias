import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    Platform
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'
import API from '../../networking/api'

class SplashClass extends Component {
    api = new API()
    constructor(props) {
        super(props);
    }

    async getToken() {
        try {
            let data = await AsyncStorage.getItem('token')
            return data
        } catch (error) {
            console.log(error);
        }
    }

    preload(){
        this.getToken()
            .then(data => {
                if(data){
                    this.api.me(data)
                        .then(customer => {
                            if(customer.status === 200 ){
                                this.props.dispatch({
                                    type: 'SET_CUSTOMER', value: {
                                        token: customer.data.token,
                                        id: customer.data.id,
                                        new_chat: customer.data.new_chat,
                                        email: customer.data.email,
                                        church_name: customer.data.church_name,
                                        member_church_name: customer.data.church_name,
                                        member_church_id: customer.data.church_id,
                                        first_name: customer.data.first_name,
                                        last_name: customer.data.last_name,
                                        phone: customer.data.phone,
                                        photo: customer.data.photo,
                                        country: customer.data.country,
                                        role: customer.data.role,
                                    },
                                });
                                if(customer.data.role === 'church'){
                                    return this.props.navigation.navigate('ChurchNavigator')
                                }else{
                                    return this.props.navigation.navigate('CustomerNavigator')
                                }
                            }
                            this.props.navigation.navigate('LoginNavigator')
                        })
                        .catch(err => {
                            this.props.navigation.navigate('LoginNavigator')
                        })
                }else{
                    this.props.navigation.navigate('LoginNavigator')
                }
            })
            .catch(err => {
                this.props.navigation.navigate('LoginNavigator')
            })
    }

    render() {
        return (
            <View style={styles.content}>
               <Image source={require('../../assets/img/logo.png')} style={{width: 250, height: 250,   resizeMode: 'contain'}}/>
            </View>
        );
    }
    componentDidMount() {
        this.time = setTimeout(() => {
            this.preload()
        }, 1500)
    }
    componentWillUnmount() {
        clearTimeout(this.time);
    }
}

export const Splash = connect(({ config }) => ({ config }))(SplashClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fafbfd',
        justifyContent: 'center',
        alignItems: 'center'
    },

});
