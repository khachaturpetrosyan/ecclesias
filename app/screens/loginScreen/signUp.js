import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    ScrollView,
    Keyboard,
} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {Loading} from '../../components';
import API from '../../networking/api';
import AsyncStorage from "@react-native-community/async-storage";

class SignUpClass extends Component {

    api = new API();

    constructor(props) {
        super(props);
        this.state = {
            modallCountry: false,
            secure: true,
            secureConfirm: true,
            loading: false,

            firstName: '',
            lastName: '',
            phone: '',
            country: '',
            email: '',
            email_church: '',
            church_name: '',
            password: '',
            confirmPassword: '',
            role: 'customer',

            firstNameError: '',
            lastNameError: '',
            countryError: '',
            emailError: '',
            emailChurchError: '',
            nameChurchError: '',
            phoneError: '',
            mismatchPasswordError: '',
            serverError: '',
        };
        GoogleSignin.configure({
            scopes: [
                'profile',
                'email',
                'openid',
            ],
            offlineAccess: false,
            forceCodeForRefreshToken: true,
            webClientId: '1082383314653-o5d49pvn5blhgo7d58cmnkbkbgiuac26.apps.googleusercontent.com',
        });
    }

    async setToken(token) {
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {
            console.log(error);
        }
    }

    handleChange = (e, name) => {
        this.setState({
            [name]: e,
            firstNameError: '',
            lastNameError: '',
            countryError: '',
            emailError: '',
            emailChurchError: '',
            nameChurchError: '',
            phoneError: '',
            passwordError: '',
            confirmPasswordError: '',
            mismatchPasswordError: '',
            serverError: '',
        });
    };

    emailValidator() {
        let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(this.state.email);
    }

    isValidAll(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key) && obj[key]) {
                return false;
            }
        }
        return true;
    }

    google = async () => {
        Keyboard.dismiss();
        this.setState({
            loading: true,
        });
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            this.setState({userInfo});
            if (userInfo) {
                let info = {
                    google_id: userInfo.user.id,
                    email: userInfo.user.email,
                    first_name: userInfo.user.familyName,
                    last_name: userInfo.user.givenName,
                    photo: userInfo.user.photo,
                    role: 'user'
                }
                this.api.signin_google(info)
                    .then(async customer => {
                        this.setState({
                            loading: false,
                        });
                        if (customer.status === 200) {
                            await this.setToken(customer.data.token);
                            this.props.dispatch({
                                type: 'SET_CUSTOMER', value: {
                                    token: customer.data.token,
                                    id: customer.data.id,
                                    google_id: customer.data.google_id,
                                    email: customer.data.email,
                                    first_name: customer.data.first_name,
                                    last_name: customer.data.last_name,
                                    photo: customer.data.photo,
                                    role: customer.data.role,
                                },
                            });
                            this.props.navigation.replace('CustomerNavigator');
                        }
                    })
                    .catch(err => {
                        this.setState({
                            loading: false,
                        });
                    })
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    async signInWithFacebook() {
        Keyboard.dismiss();
        this.setState({
            loading: true,
        });
        if (Platform.OS === 'android') {
            LoginManager.setLoginBehavior('web_only');
        }
        await LoginManager.logInWithPermissions(['public_profile']).then(result => {
                if (result.isCancelled) {
                    this.setState({
                        loading: true,
                    });
                    console.log('Login cancelled');
                } else {
                    this.setState({
                        loading: true,
                    });
                    console.log(
                        'Login success with permissions: ' +
                        result.grantedPermissions.toString(),
                    );
                    return AccessToken.getCurrentAccessToken().then(({accessToken}) => {
                        return fetch(
                            `https://graph.facebook.com/me?fields=email,first_name,last_name,birthday,picture.width(100).height(100)&access_token=${accessToken}`)
                            .then((response) => response.json())
                            .then((userInfo) => {
                                let info = {
                                    facebook_id: userInfo.id,
                                    first_name: userInfo.first_name,
                                    last_name: userInfo.last_name,
                                    photo: userInfo.picture.data.uri,
                                    role: 'user'
                                }
                                this.api.signin_face(info)
                                    .then(async customer => {
                                        this.setState({
                                            loading: false,
                                        });
                                        if (customer.status === 200) {
                                            await this.setToken(customer.data.token);
                                            this.props.dispatch({
                                                type: 'SET_CUSTOMER', value: {
                                                    token: customer.data.token,
                                                    id: customer.data.id,
                                                    face_id: customer.data.face_id,
                                                    email: customer.data.email,
                                                    first_name: customer.data.first_name,
                                                    last_name: customer.data.last_name,
                                                    photo: customer.data.photo,
                                                    role: customer.data.role,
                                                },
                                            });
                                        }
                                    })
                                    .catch(err => {
                                        this.setState({
                                            loading: false,
                                        });
                                    })
                            });
                    })
                        .catch((error) => {
                            this.setState({
                                loading: true,
                            });
                            console.log(error);
                        });
                }
            },
            function (error) {
                console.log('Login fail with error: ' + error);
            },
        ).catch((error) => {
            console.log(error);

        });
    }

    submit() {
        Keyboard.dismiss();
        let errors = {
            firstNameError: this.state.firstName ? '' : 'Invalid first name',
            lastNameError: this.state.lastName ? '' : 'Invalid first name',
            countryError: this.state.country ? '' : 'Select Country',
            emailError: this.emailValidator() ? '' : 'Email Invalid',
            phoneError: this.state.phone ? '' : 'Email Invalid',
            passwordError: this.state.password ? '' : 'Password Invalid',
            confirmPasswordError: this.state.confirmPassword ? '' : 'Confirm Password Invalid',
        };
        if (this.isValidAll(errors)) {
            if (this.state.password === this.state.confirmPassword) {
                this.createCustomer();
            } else {
                this.setState({
                    mismatchPasswordError: 'Password mismatch',
                });
            }
        } else {
            this.setState({
                ...errors,
            });
        }
    }

    submitchurch() {
        Keyboard.dismiss();
        let errors = {
            firstNameError: this.state.firstName ? '' : 'Invalid first name',
            lastNameError: this.state.lastName ? '' : 'Invalid first name',
            countryError: this.state.country ? '' : 'Select Country',
            emailError: this.emailValidator() ? '' : 'Email Invalid',
            emailChurchError: this.emailValidator() ? '' : 'Email Church Invalid',
            nameChurchError: this.emailValidator() ? '' : 'Church Name Invalid',
            phoneError: this.state.phone ? '' : 'Email Invalid',
            passwordError: this.state.password ? '' : 'Password Invalid',
            confirmPasswordError: this.state.confirmPassword ? '' : 'Confirm Password Invalid',
        };
        if (this.isValidAll(errors)) {
            if (this.state.password === this.state.confirmPassword) {
                this.createChurch();
            } else {
                this.setState({
                    mismatchPasswordError: 'Password mismatch',
                });
            }
        } else {
            this.setState({
                ...errors,
            });
        }
    }

    createCustomer() {
        this.setState({
            loading: true
        });
        let data = {
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            phone: this.state.phone,
            country: this.state.country,
            email: this.state.email,
            password: this.state.password,
            confirm_password: this.state.confirmPassword,
            role: this.props.route.params.role
        };
        this.api.signup(data)
            .then(data => {
                if (data.status === 200) {
                    this.setState({
                        loading: false
                    });
                    return this.props.navigation.navigate('SignIn', {role: this.props.route.params.role})
                }
                if (data.status === 411) {
                    return this.setState({
                        loading: false,
                        serverError: 'Customer by this email already exists',
                    });
                }
                this.setState({
                    loading: false,
                    serverError: 'Please try again later',
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loading: false,
                    serverError: 'Please try again later',
                });
            });
    }

    createChurch() {
        this.setState({
            loading: true
        });
        let data = {
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            phone: this.state.phone,
            country: this.state.country,
            email: this.state.email,
            email_church: this.state.email_church,
            name: this.state.church_name,
            password: this.state.password,
            confirm_password: this.state.confirmPassword,
            role: this.props.route.params.role
        };
        this.api.signupchurch(data)
            .then(data => {
                if (data.status === 200) {
                    this.setState({
                        loading: false
                    });
                    return this.props.navigation.navigate('GetStarted')
                }
                if (data.status === 411) {
                    return this.setState({
                        loading: false,
                        serverError: 'Customer by this email already exists',
                    });
                }
                this.setState({
                    loading: false,
                    serverError: 'Please try again later',
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loading: false,
                    serverError: 'Please try again later',
                });
            });
    }

    _rendercountry() {
        if (this.state.modallCountry) {
            return (
                <CountryPicker
                    countryCode={'AM'}
                    onSelect={(value) => {
                        this.setState({
                            country: value.name,
                        });
                    }}
                    translation='eng'
                    visible={true}
                    withCallingCode={true}
                    withFilter={true}
                    withFlag={true}
                    onClose={() => {
                        this.setState({modallCountry: false});
                    }}
                />
            );
        }
    }

    render() {
        return (
            <View style={styles.content}>
                <ScrollView keyboardShouldPersistTaps='always' showsVerticalScrollIndicator={false}>
                    <View style={styles.heder_view}>
                        <TouchableOpacity style={styles.back_view} onPress={() => this.props.navigation.goBack()}>
                            <Image source={require('../../assets/img/back.png')} style={{width: 24, height: 24}}/>
                            <Text>back</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.form_view}>
                        <TextInput
                            placeholder="First Name"
                            placeholderTextColor="#000"
                            onChangeText={(e) => this.handleChange(e, 'firstName')}
                            style={[styles.input, this.state.firstNameError ?
                                {backgroundColor: '#f8e6ed'} : null]}
                        />
                        <View style={styles.err_view}>
                            {this.state.firstNameError ?
                                <Text style={styles.err_text}>{this.state.firstNameError}</Text> : null}
                        </View>
                        <TextInput
                            placeholder="Last Name"
                            placeholderTextColor="#000"
                            onChangeText={(e) => this.handleChange(e, 'lastName')}
                            style={[styles.input, this.state.lastNameError ?
                                {backgroundColor: '#f8e6ed'} : null]}
                        />
                        <View style={styles.err_view}>
                            {this.state.lastNameError ?
                                <Text style={styles.err_text}>{this.state.lastNameError}</Text> : null}
                        </View>
                        <TextInput
                            placeholder="Phone Number"
                            keyboardType={'phone-pad'}
                            placeholderTextColor="#000"
                            onChangeText={(e) => this.handleChange(e, 'phone')}
                            style={[styles.input, this.state.phoneError ?
                                {backgroundColor: '#f8e6ed'} : null]}
                        />
                        <View style={styles.err_view}>
                            {this.state.phoneError ?
                                <Text style={styles.err_text}>{this.state.phoneError}</Text> : null}
                        </View>
                        <TouchableOpacity style={[styles.password_view, this.state.countryError ?
                            {backgroundColor: '#f8e6ed'} : null]}
                                          onPress={() => this.setState({modallCountry: true})}>
                            <TextInput
                                editable={false}
                                selectTextOnFocus={false}
                                placeholder="Country"
                                value={this.state.country}
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'country')}
                                style={styles.password_input}
                            />
                            <View>
                                <Image source={require('../../assets/img/down.png')}
                                       style={{width: 20, resizeMode: 'contain', height: 20}}/>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.err_view}>
                            {this.state.countryError ?
                                <Text style={styles.err_text}>{this.state.countryError}</Text> : null}
                        </View>
                        <TextInput
                            placeholder="Email Adress"
                            keyboardType="email-address"
                            placeholderTextColor="#000"
                            onChangeText={(e) => this.handleChange(e, 'email')}
                            style={[styles.input, this.state.emailError ?
                                {backgroundColor: '#f8e6ed'} : null]}
                        />
                        <View style={styles.err_view}>
                            {this.state.emailError ?
                                <Text style={styles.err_text}>{this.state.emailError}</Text> : null}
                        </View>
                        {this.props.route.params.role === 'church' ?
                            <>
                                <TextInput
                                    placeholder="Email Adress Church"
                                    keyboardType="email-address"
                                    placeholderTextColor="#000"
                                    onChangeText={(e) => this.handleChange(e, 'email_church')}
                                    style={[styles.input, this.state.emailChurchError ?
                                        {backgroundColor: '#f8e6ed'} : null]}
                                />
                                <View style={styles.err_view}>
                                    {this.state.emailChurchError ?
                                        <Text style={styles.err_text}>{this.state.emailChurchError}</Text> : null}
                                </View>
                            </>
                            :
                            null
                        }
                        {this.props.route.params.role === 'church' ?
                            <>
                                <TextInput
                                    placeholder="Church Name"
                                    placeholderTextColor="#000"
                                    onChangeText={(e) => this.handleChange(e, 'church_name')}
                                    style={[styles.input, this.state.nameChurchError ?
                                        {backgroundColor: '#f8e6ed'} : null]}
                                />
                                <View style={styles.err_view}>
                                    {this.state.nameChurchError ?
                                        <Text style={styles.err_text}>{this.state.nameChurchError}</Text> : null}
                                </View>
                            </>
                            :
                            null
                        }

                        <View style={[styles.password_view, this.state.passwordError ?
                            {backgroundColor: '#f8e6ed'} : null]}>
                            <TextInput
                                secureTextEntry={this.state.secure}
                                placeholder="Password"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'password')}
                                style={styles.password_input}
                            />
                            <TouchableOpacity onPress={() => this.setState({secure: !this.state.secure})}>
                                {this.state.secure ?
                                    < Image source={require('../../assets/img/closEyes.png')}
                                            style={{width: 24, height: 24}}/>
                                    :
                                    <Image source={require('../../assets/img/eyes.png')}
                                           style={{width: 24, height: 24}}/>
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.err_view}>
                            {this.state.passwordError ?
                                <Text style={styles.err_text}>{this.state.passwordError}</Text> : null}
                        </View>
                        <View style={[styles.password_view, this.state.confirmPasswordError ?
                            {backgroundColor: '#f8e6ed'} : null]}>
                            <TextInput
                                secureTextEntry={this.state.secureConfirm}
                                placeholder="Confirm Password"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'confirmPassword')}
                                style={styles.password_input}
                            />
                            <TouchableOpacity onPress={() => this.setState({secureConfirm: !this.state.secureConfirm})}>
                                {this.state.secureConfirm ?
                                    < Image source={require('../../assets/img/closEyes.png')}
                                            style={{width: 24, height: 24}}/>
                                    :
                                    <Image source={require('../../assets/img/eyes.png')}
                                           style={{width: 24, height: 24}}/>
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.err_view}>
                            {this.state.confirmPasswordError ?
                                <Text style={styles.err_text}>{this.state.confirmPasswordError}</Text> : null}
                            {this.state.mismatchPasswordError ?
                                <Text style={styles.err_text}>{this.state.mismatchPasswordError}</Text> : null}
                            {this.state.serverError ?
                                <Text style={styles.err_text}>{this.state.serverError}</Text> : null}

                        </View>
                        {this.props.route.params.role === 'user' ?
                            <View style={styles.google_face_btn_view}>
                                <TouchableOpacity style={[styles.google_face_btn, {backgroundColor: '#3B5998'}]}
                                                  onPress={() => this.signInWithFacebook()}>
                                    <Image source={require('../../assets/img/face.png')}
                                           style={{width: 21, resizeMode: 'contain', height: 21}}/>
                                    <Text style={[styles.google_face_btn_text, {color: '#fff'}]}>Facebook</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.google_face_btn} onPress={() => this.google()}>
                                    <Image source={require('../../assets/img/google.png')}
                                           style={{width: 21, resizeMode: 'contain', height: 21}}/>
                                    <Text style={styles.google_face_btn_text}>Google</Text>
                                </TouchableOpacity>
                            </View>
                            :
                            null}
                        <TouchableOpacity style={styles.sign_up_btn} onPress={() => {
                            if (this.props.route.params.role === 'church') {
                                this.submitchurch()
                            } else {
                                this.submit()
                            }
                        }}>
                            <Text style={styles.sign_up_btn_text}>Sign Up</Text>
                        </TouchableOpacity>
                        {this._rendercountry()}
                    </View>
                </ScrollView>
                <Loading loading={this.state.loading}/>
            </View>
        );
    }
}

export const SignUp = SignUpClass;

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    heder_view: {
        marginTop: 19,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 23,
    },
    back_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    back_text: {
        marginLeft: 6,
        fontSize: 12,
        lineHeight: 16,
        color: '#000000',
        fontFamily: 'SegoeUI',
    },
    form_view: {
        marginTop: 50,
        paddingHorizontal: 23,
    },
    input: {
        height: 40,
        paddingLeft: 10,
        color: '#000',
        borderWidth: 1,
        borderColor: '#9F9F9F',
        borderRadius: 15,
    },
    password_view: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        borderWidth: 1,
        paddingRight: 10,
        borderColor: '#9F9F9F',
        borderRadius: 15,
    },
    password_input: {
        width: '80%',
        height: 40,
        paddingLeft: 10,
        color: '#000',
    },
    err_view: {
        height: 20,
        marginVertical: 5,
    },
    err_text: {
        color: 'red',
        fontFamily: 'SegoeUI',
        fontSize: 16,
        lineHeight: 21,
        marginLeft: 5,
    },
    google_face_btn_view: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    google_face_btn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#3B5998',
        height: 45,
        width: '48%',
        borderRadius: 16,
    },
    google_face_btn_text: {
        color: '#000',
        fontSize: 16,
        lineHeight: 21,
        marginLeft: 15,
        fontFamily: 'SegoeUI',
    },
    sign_up_btn: {
        marginBottom: 30,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        backgroundColor: '#fff300',
        height: 45,
    },
    sign_up_btn_text: {
        color: '#000',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI',
    },
});
