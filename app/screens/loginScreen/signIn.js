import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    ScrollView,
    Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {Loading} from '../../components';
import API from '../../networking/api';


class SignInClass extends Component {

    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            secure: true,
            loading: false,

            email: '',
            password: '',

            serverError: '',
            emailError: '',
            passwordError: '',
        };
        GoogleSignin.configure({
            scopes: [
                'profile',
                'email',
                'openid',
            ],
            offlineAccess: false,
            forceCodeForRefreshToken: true,
            webClientId: '1082383314653-o5d49pvn5blhgo7d58cmnkbkbgiuac26.apps.googleusercontent.com',
        });
    }

    async setToken(token) {
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {
            console.log(error);
        }
    }

    handleChange = (e, name) => {
        this.setState({
            [name]: e,
            emailError: '',
            passwordError: '',
            serverError: '',
        });
    };

    google = async () => {
        Keyboard.dismiss();
        this.setState({
            loading: true,
        });
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            this.setState({userInfo});
            if (userInfo) {
                let info = {
                    google_id: userInfo.user.id,
                    email: userInfo.user.email,
                    first_name: userInfo.user.familyName,
                    last_name: userInfo.user.givenName,
                    photo: userInfo.user.photo,
                    role: 'user'
                }
                this.api.signin_google(info)
                    .then(async customer => {
                        this.setState({
                            loading: false,
                        });
                        if (customer.status === 200) {
                            await this.setToken(customer.data.token);
                            this.props.dispatch({
                                type: 'SET_CUSTOMER', value: {
                                    token: customer.data.token,
                                    id: customer.data.id,
                                    google_id: customer.data.google_id,
                                    email: customer.data.email,
                                    first_name: customer.data.first_name,
                                    last_name: customer.data.last_name,
                                    photo: customer.data.photo,
                                    country: customer.data.country,
                                    role: customer.data.role,
                                },
                            });
                            this.props.navigation.replace('CustomerNavigator');
                        }
                    })
                    .catch(err => {
                        this.setState({
                            loading: false,
                        });
                    })
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    async signInWithFacebook() {
        Keyboard.dismiss();
        this.setState({
            loading: true,
        });
        if (Platform.OS === 'android') {
            LoginManager.setLoginBehavior('web_only');
        }
        await LoginManager.logInWithPermissions(['public_profile']).then(result => {
                if (result.isCancelled) {
                    console.log('Login cancelled');
                } else {
                    console.log(
                        'Login success with permissions: ' +
                        result.grantedPermissions.toString(),
                    );
                    return AccessToken.getCurrentAccessToken().then(({accessToken}) => {
                        return fetch(
                            `https://graph.facebook.com/me?fields=email,first_name,last_name,birthday,picture.width(100).height(100)&access_token=${accessToken}`)
                            .then((response) => response.json())
                            .then((userInfo) => {
                                let info = {
                                    facebook_id: userInfo.id,
                                    first_name: userInfo.first_name,
                                    last_name: userInfo.last_name,
                                    photo: userInfo.picture.data.uri,
                                    role: 'user'
                                }
                                this.api.signin_face(info)
                                    .then(async customer => {
                                        this.setState({
                                            loading: false,
                                        });
                                        if (customer.status === 200) {
                                            await this.setToken(customer.data.token);
                                            this.props.dispatch({
                                                type: 'SET_CUSTOMER', value: {
                                                    token: customer.data.token,
                                                    id: customer.data.id,
                                                    face_id: customer.data.face_id,
                                                    email: customer.data.email,
                                                    first_name: customer.data.first_name,
                                                    last_name: customer.data.last_name,
                                                    photo: customer.data.photo,
                                                    country: customer.data.country,
                                                    role: customer.data.role,
                                                },
                                            });
                                        }
                                    })
                                    .catch(err => {
                                        this.setState({
                                            loading: false,
                                        });
                                    })
                            });
                    })
                        .catch((error) => {
                            console.log(error);

                        });
                }
            },
            function (error) {
                console.log('Login fail with error: ' + error);
            },
        ).catch((error) => {
            console.log(error);

        });
    }

    emailValidator() {
        let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(this.state.email);
    }

    isValidAll(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key) && obj[key]) {
                return false;
            }
        }
        return true;
    }

    submit() {
        Keyboard.dismiss();
        let errors = {
            emailError: this.emailValidator() ? '' : 'Email Invalid',
            passwordError: this.state.password ? '' : 'Password Invalid',
        };
        if (this.isValidAll(errors)) {
            if (this.state.password.length > 7) {
                this.loginCustomer();
            } else {
                this.setState({
                    mismatchPasswordError: 'Password mismatch',
                });
            }
        } else {
            this.setState({
                ...errors,
            });
        }
    }

    loginCustomer() {
        this.setState({
           loading: true,
        });
        let data = {
            email: this.state.email,
            password: this.state.password,
            role: this.props.route.params.role
        };
        this.api.signin(data)
            .then(async customer => {
                if (customer.status === 404 || customer.status === 401) {
                    return this.setState({
                        loading: false,
                        serverError: customer.data.message,
                    })
                }
                if (customer.status === 200) {
                    await this.setToken(customer.data.token);
                    this.props.dispatch({
                        type: 'SET_CUSTOMER', value: {
                            token: customer.data.token,
                            id: customer.data.id,
                            email: customer.data.email,
                            first_name: customer.data.first_name,
                            last_name: customer.data.last_name,
                            church_name: customer.data.church_name,
                            phone: customer.data.phone,
                            photo: customer.data.photo,
                            country: customer.data.country,
                            role: customer.data.role,
                        },
                    });
                    this.setState({
                        loading: false,
                    })
                    if (customer.data.role === 'church') {
                        return this.props.navigation.replace('ChurchNavigator')
                    }
                    return this.props.navigation.replace('CustomerNavigator')
                }
                this.setState({
                    loading: false,
                    serverError: 'Please try again later',
                });
            })
            .catch(err => {
                this.setState({
                    loading: false,
                    serverError: 'Please try again later',
                });
            });
    }

    render() {
        return (
            <View style={styles.content}>
                <ScrollView keyboardShouldPersistTaps='always' showsVerticalScrollIndicator={false}>
                    <View style={styles.header_view}>
                        <TouchableOpacity style={styles.back_view} onPress={() => this.props.navigation.goBack()}>
                            <Image source={require('../../assets/img/back.png')} style={{width: 24, height: 24}}/>
                            <Text>back</Text>
                        </TouchableOpacity>
                        <Text style={styles.login_text}>Login</Text>
                    </View>
                    <View style={styles.form_view}>
                        <TextInput
                            placeholder="Email"
                            placeholderTextColor="#000"
                            onChangeText={(e) => this.handleChange(e, 'email')}
                            style={[styles.input, this.state.emailError ?
                                {backgroundColor: '#f8e6ed'} : null]}
                        />
                        <View style={styles.err_view}>
                            {this.state.emailError ?
                                <Text style={styles.err_text}>{this.state.emailError}</Text> : null}
                        </View>
                        <View style={[styles.password_view, this.state.passwordError ?
                            {backgroundColor: '#f8e6ed'} : null]}>
                            <TextInput
                                secureTextEntry={this.state.secure}
                                placeholder="Password"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'password')}
                                style={styles.password_input}
                            />
                            <TouchableOpacity onPress={() => this.setState({secure: !this.state.secure})}>
                                {this.state.secure ?
                                    < Image source={require('../../assets/img/closEyes.png')}
                                            style={{width: 24, height: 24}}/>
                                    :
                                    <Image source={require('../../assets/img/eyes.png')}
                                           style={{width: 24, height: 24}}/>
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.err_view}>
                            {this.state.passwordError ?
                                <Text style={styles.err_text}>{this.state.passwordError}</Text> : null}
                            {this.state.serverError ?
                                <Text style={styles.err_text}>{this.state.serverError}</Text> : null}
                        </View>
                        <View style={styles.forgot_view}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ResetPassword')}>
                                <Text style={styles.forgot_text}>Forgot Password?</Text>
                            </TouchableOpacity>
                        </View>
                        {this.props.route.params.role === 'user' ?
                            <View style={styles.google_face_btn_view}>
                                <TouchableOpacity style={[styles.google_face_btn, {backgroundColor: '#3B5998'}]}
                                                  onPress={() => {
                                                      this.signInWithFacebook();
                                                  }}>
                                    <Image source={require('../../assets/img/face.png')}
                                           style={{width: 21, resizeMode: 'contain', height: 21}}/>
                                    <Text style={[styles.google_face_btn_text, {color: '#fff'}]}>Facebook</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.google_face_btn} onPress={() => this.google()}>
                                    <Image source={require('../../assets/img/google.png')}
                                           style={{width: 21, resizeMode: 'contain', height: 21}}/>
                                    <Text style={styles.google_face_btn_text}>Google</Text>
                                </TouchableOpacity>
                            </View>
                            :
                            null}
                        <TouchableOpacity style={styles.sign_in_btn} onPress={() => {
                            this.submit()
                        }}>
                            <Text style={styles.sign_in_btn_text}>Sign In</Text>
                        </TouchableOpacity>
                        <View style={styles.sign_up_view}>
                            <TouchableOpacity>
                                <Text style={styles.sign_up_text}>Don't have an account? <Text
                                    style={[styles.sign_up_text, {color: '#FFA41B', fontFamily: 'SegoeUI-Bold'}]}>Sign
                                    Up</Text></Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <Loading loading={this.state.loading}/>
            </View>
        );
    }
}

export const SignIn = connect(({config, customer}) => ({config, customer}))(SignInClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header_view: {
        marginTop: 19,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 23,
    },
    back_view: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    back_text: {
        marginLeft: 6,
        fontSize: 12,
        lineHeight: 16,
        color: '#000000',
        fontFamily: 'SegoeUI',
    },
    login_text: {
        marginLeft: 6,
        fontSize: 16,
        color: '#9F9F9F',
        lineHeight: 16,
        fontFamily: 'SegoeUI',
    },
    form_view: {
        marginTop: 50,
        paddingHorizontal: 23,
    },
    input: {
        height: 40,
        paddingLeft: 10,
        color: '#000',
        borderWidth: 1,
        borderColor: '#9F9F9F',
        borderRadius: 15,
    },
    err_view: {
        height: 20,
        marginVertical: 5,
    },
    err_text: {
        color: 'red',
        fontFamily: 'SegoeUI',
        fontSize: 16,
        lineHeight: 21,
        marginLeft: 5,
    },
    password_view: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        borderWidth: 1,
        paddingRight: 10,
        borderColor: '#9F9F9F',
        borderRadius: 15,
    },
    password_input: {
        width: '80%',
        height: 40,
        paddingLeft: 10,
        color: '#000',
    },
    forgot_view: {
        width: '100%',
        alignItems: 'flex-end',
    },
    forgot_text: {
        color: '#395AFF',
        lineHeight: 20,
        fontSize: 15,
        fontFamily: 'SegoeUI-Bold',
    },
    google_face_btn_view: {
        marginTop: 35,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    google_face_btn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#3B5998',
        height: 45,
        width: '48%',
        borderRadius: 16,
    },
    google_face_btn_text: {
        color: '#000',
        fontSize: 16,
        lineHeight: 21,
        marginLeft: 15,
        fontFamily: 'SegoeUI',
    },
    sign_in_btn: {
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        backgroundColor: '#fff300',
        height: 45,
    },
    sign_in_btn_text: {
        color: '#000',
        fontSize: 20,
        lineHeight: 27,
        fontFamily: 'SegoeUI',
    },
    sign_up_view: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
        paddingBottom: 30,
    },
    sign_up_text: {
        color: '#000000',
        fontFamily: 'SegoeUI',
        fontSize: 14,
        lineHeight: 19,
    },
});
