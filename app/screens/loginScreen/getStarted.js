import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'

class GetStartedClass extends Component {
    constructor(props) {
        super(props);
    }

    async getToken() {
        try {
            let data = await AsyncStorage.getItem('user')
            return data
        } catch (error) {
            console.log(error);
        }
    }

    async getOpened() {
        try {
            let data = await AsyncStorage.getItem('opened')
            return data
        } catch (error) {
            console.log(error);
        }
    }



    render() {
        return (
            <View style={styles.content}>
              <View style={styles.title_view}>
                  <Image source={require('../../assets/img/logo.png')} style={{width: '80%', height: 90}}/>
                  <Text style={[styles.title_info_text, {marginTop: 17}]}>To be in the world and note be affected</Text>
                  <Text style={styles.title_info_text}>by the world</Text>
              </View>
                <View style={styles.btn_view}>
                    <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('ChooseSignInUp', {role: 'church'})}>
                        <Text style={styles.btn_text}>Church</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btn, {backgroundColor: '#fff300', borderWidth: 0}]} onPress={() => this.props.navigation.navigate('ChooseSignInUp', {role: 'customer'})}>
                        <Text style={[styles.btn_text, {color: '#000'}]}>Member</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export const GetStarted = connect(({ config }) => ({ config }))(GetStartedClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'space-between',
    },
    title_view: {
        alignItems: 'center',
        marginTop: 150
    },
    title_info_text: {
        fontFamily: 'SegoeUI',
        fontSize: 12,
        lineHeight: 16,
        color: '#9F9F9F',
    },
    btn_view: {
        paddingBottom: 30,
    },
    btn: {
        borderWidth: 1,
        borderColor: '#37ADDB',
        borderRadius: 15,
        marginTop: 15,
        backgroundColor: '#fff',
        width: '80%',
        alignItems:  'center',
        justifyContent: 'center',
        height: 45,
        marginHorizontal: '10%'
    },
    btn_text: {
        fontFamily: 'SegoeUI',
        fontSize: 18,
        color: '#37ADDB',
        lineHeight: 24
    }
});
