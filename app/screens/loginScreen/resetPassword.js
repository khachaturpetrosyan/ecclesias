import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    TextInput,
} from 'react-native';
import CodeInput from 'react-native-code-input';
import {Header} from '../../components/header';
import API from '../../networking/api'


class ResetPasswordClass extends Component {

    api = new API()

    constructor(props) {
        super(props);
        this.state = {
            resetPassword: true,
            code: false,
            newPassword: false,
            secure: true,
            loading: false,
            email: '',
            pin: '',
            password: '',
            confirmPassword: '',

            emailError: '',
        };
    }

    handleChange = (e, name) => {
        this.setState({
            [name]: e,
            emailError: ''
        });
    };

    emailValidator() {
        let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(this.state.email);
    }

    sendEmail() {
        this.setState({
            loading: true
        })
        if (this.emailValidator()) {
            let info = {
                email: this.state.email
            }
            this.api.reset_password_email(info)
                .then(data => {
                    this.setState({
                        loading: false
                    })
                    if (data.status === 404) {
                        this.setState({
                            emailError: 'Email not found',
                        })
                    }
                    if (data.status === 400) {
                        this.setState({
                            emailError: 'Email incorect',
                        })
                    }
                    if (data.status === 200) {
                        this.setState({
                            resetPassword: false,
                            code: true,
                        });
                    }
                })
                .catch(err => {
                    this.setState({
                        loading: false
                    })
                })
        } else {
            this.setState({
                emailError: 'Email Invalid',
            })
        }
    }

    sendCode() {
        this.setState({
            loading: true
        })
        let info = {
            email: this.state.email,
            code: this.state.pin
        }
        this.api.reset_password_code(info)
            .then(data => {
                this.setState({
                    loading: false
                })
                if (data.status === 404) {
                    this.setState({
                        emailError: 'Email not found',
                    })
                }
                if (data.status === 403) {
                    this.setState({
                        emailError: 'Code incorect',
                    })
                }
                if (data.status === 200) {
                    this.setState({
                        code: false,
                        newPassword: true,
                    });
                }
            })
            .catch(err => {
                this.setState({
                    loading: false
                })
            })
    }

    sendPassword() {
        this.setState({
            loading: true
        })
        if( this.state.password ===  this.state.confirmPassword){
            let info = {
                email: this.state.email,
                password: this.state.password
            }
            this.api.reset_password_change(info)
                .then(data => {
                    this.setState({
                        loading: false
                    })
                    if (data.status === 404) {
                        this.setState({
                            emailError: 'Email not found',
                        })
                    }
                    if (data.status === 403) {
                        this.setState({
                            emailError: 'Code incorect',
                        })
                    }
                    if (data.status === 400) {
                        this.setState({
                            emailError: 'Password incorect',
                        })
                    }
                    if (data.status === 200) {
                        this.props.navigation.navigate('SignIn')
                    }
                })
                .catch(err => {
                    this.setState({
                        loading: false
                    })
                })
        }else{
            this.setState({
                emailError: 'Password mismatch'
            })
        }
    }

    render() {
        return (
            <View style={styles.content}>
                <Header
                    back={true}
                    navigation={this.props.navigation}
                />
                {this.state.resetPassword ? <View style={styles.cont}>
                        <Text style={styles.title_text}>Reset your Password</Text>
                        <TextInput
                            keyboardType="email-address"
                            placeholder={'Enter email'}
                            style={[styles.input, this.state.emailError ?
                                {backgroundColor: '#f8e6ed'} : null]}
                            onChangeText={(e) => this.handleChange(e, 'email')}
                        />
                        <View style={styles.error_view}>
                            {this.state.emailError ?
                                <Text style={styles.error_text}>{this.state.emailError}</Text>
                                :
                                null}
                        </View>
                        <TouchableOpacity style={styles.btn} onPress={() => {
                            this.sendEmail()
                        }}>
                            <Text style={styles.btn_text}>Get the code</Text>
                        </TouchableOpacity>
                    </View>
                    : null}
                {this.state.code ? <View style={styles.cont}>
                    <Text style={styles.title_text}>Enter the received code</Text>
                    <View style={{height: 80}}>
                        <CodeInput
                            ref="codeInputRef2"
                            activeColor='black'
                            inactiveColor='rgba(49, 180, 4, 1.3)'
                            autoFocus={false}
                            inputPosition='center'
                            size={50}
                            codeLength={5}
                            onFulfill={(pin) => {
                                this.handleChange(pin, 'pin');
                            }}
                            containerStyle={{marginTop: 30}}
                            codeInputStyle={styles.code_item}
                        />
                    </View>
                    <View style={styles.error_view}>
                        {this.state.emailError ?
                            <Text style={styles.error_text}>{this.state.emailError}</Text>
                            :
                            null}
                    </View>
                    <TouchableOpacity style={styles.btn} onPress={() => this.sendCode()}>
                        <Text style={styles.btn_text}>Reset Password</Text>
                    </TouchableOpacity>
                </View> : null}
                {this.state.newPassword ?
                    <View style={styles.cont}>
                        <Text style={styles.title_text}>Reset your Password</Text>
                        <View style={styles.password_view}>
                            <TextInput
                                secureTextEntry={this.state.secure}
                                placeholder="Password"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'password')}
                                style={styles.password_input}
                            />
                            <TouchableOpacity onPress={() => this.setState({secure: !this.state.secure})}>
                                {this.state.secure ?
                                    < Image source={require('../../assets/img/closEyes.png')}
                                            style={{width: 24, height: 24}}/>
                                    :
                                    <Image source={require('../../assets/img/eyes.png')}
                                           style={{width: 24, height: 24}}/>
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.password_view}>
                            <TextInput
                                secureTextEntry={this.state.secure}
                                placeholder="Confirm Password"
                                placeholderTextColor="#000"
                                onChangeText={(e) => this.handleChange(e, 'confirmPassword')}
                                style={styles.password_input}
                            />
                            <TouchableOpacity onPress={() => this.setState({secure: !this.state.secure})}>
                                {!this.state.secure ?
                                    < Image source={require('../../assets/img/closEyes.png')}
                                            style={{width: 24, height: 24}}/>
                                    :
                                    <Image source={require('../../assets/img/eyes.png')}
                                           style={{width: 24, height: 24}}/>
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.error_view}>
                            {this.state.emailError ?
                                <Text style={styles.error_text}>{this.state.emailError}</Text>
                                :
                                null}
                        </View>
                        <TouchableOpacity style={styles.btn} onPress={() => this.sendPassword()}>
                            <Text style={styles.btn_text}>Reset Password</Text>
                        </TouchableOpacity>
                    </View> : null}
            </View>
        );
    }

}

export const ResetPassword = ResetPasswordClass;

const styles = StyleSheet.create({
        content: {
            flex: 1,
            backgroundColor: '#fff',
        },
        cont: {
            alignItems: 'center',
        },
        title_text: {
            marginTop: 22,
            color: '#000',
            fontSize: 30,
            lineHeight: 40,
            fontFamily: 'SegoeUI-Bold',
        },
        input: {
            height: 40,
            borderWidth: 1,
            borderColor: '#9F9F9F',
            width: '90%',
            marginTop: 35,
            paddingLeft: 10,
            borderRadius: 15,
        },
        error_view: {
            marginTop: 10,
            width: '90%',
            height: 20
        },
        error_text: {
            color: 'red',
            fontSize: 14,
            lineHeight: 18,
        },
        btn: {
            marginTop: 10,
            width: '90%',
            height: 45,
            backgroundColor: '#FFF300',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 10,
        },
        btn_text: {
            fontSize: 20,
            lineHeight: 27,
            color: '#000',
        },
        code_item: {
            fontSize: 25,
            borderRadius: 12,
            backgroundColor: '#fff',
            overflow: 'hidden',
            shadowColor: '#000',
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.20,
            shadowRadius: 1.41,
            elevation: 2,
        },
        password_view: {
            marginTop: 25,
            flexDirection: 'row',
            alignItems: 'center',
            width: '90%',
            justifyContent: 'space-between',
            borderWidth: 1,
            paddingRight: 10,
            borderColor: '#9F9F9F',
            borderRadius: 15,
        },
        password_input: {
            width: '80%',
            height: 40,
            paddingLeft: 10,
            color: '#000',
        },
    }
);
