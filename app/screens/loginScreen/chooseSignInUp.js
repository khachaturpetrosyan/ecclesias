import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';

class ChooseSignInUpClass extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <ImageBackground style={styles.content} source={require('../../assets/img/chooseSignInUp.png')}>
                <View style={styles.title_view}>
                    <Text style={styles.title_text}>The Ecclesias</Text>
                    <Text style={[styles.title_info_text, {marginTop: 17}]}>AN ENVIRONMENT FOR GROWTH IN SPIRITUALITY</Text>
                   
                </View>
                <View style={styles.btn_view}>
                    <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('SignUp', {role: this.props.route.params.role})}>
                        <Text style={styles.btn_text}>Sign Up</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btn,{backgroundColor: '#fff300', borderWidth: 0}]} onPress={() => this.props.navigation.navigate('SignIn', {role: this.props.route.params.role})}>
                        <Text style={[styles.btn_text, {color: '#000'}]}>Log In</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }
}

export const ChooseSignInUp = connect(({config}) => ({config}))(ChooseSignInUpClass);

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'space-between',
    },
    title_view: {
        alignItems: 'center',
        marginTop: 150,
    },
    title_text: {
        color: '#FFD500',
        fontSize: 42,
        fontFamily: 'SegoeUI-Bold',
    },
    title_info_text: {
        fontFamily: 'SegoeUI',
        fontSize: 12,
        lineHeight: 16,
        color: '#9F9F9F',
    },
    btn_view: {
        paddingBottom: 30,
    },
    btn: {
        borderRadius: 15,
        marginTop: 15,
        backgroundColor: '#fff',
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 45,
        marginHorizontal: '10%',
    },
    btn_text: {
        fontFamily: 'SegoeUI',
        fontSize: 18,
        color: '#37ADDB',
        lineHeight: 24,
    },
});
