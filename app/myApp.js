import React, {Component} from 'react';
import {
    Platform,
    StatusBar,
    KeyboardAvoidingView
} from 'react-native'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import MainNavigator from './navigators/mainNavigator'
import {socket} from './networking/socket'
import {connect} from "react-redux";



class MyApp extends Component {
    render() {
        return (
            <SafeAreaProvider>
                <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                    <KeyboardAvoidingView
                        style={{ flex: 1 }}
                        behavior="padding"
                        enabled={Platform.OS === 'ios'}>

                            {Platform.OS === 'android' ? <StatusBar backgroundColor="#fff" barStyle={'dark-content'} /> : null}
                            <MainNavigator />
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </SafeAreaProvider>
        );
    }
    componentWillUnmount() {
        socket.emit('delete', {email: this.props.customer.email})
    }
}


export default connect(({customer}) => ({customer}))(MyApp);
