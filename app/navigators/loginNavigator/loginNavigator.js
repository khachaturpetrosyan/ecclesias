import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
    GetStarted,
    ChooseSignInUp,
    SignIn,
    SignUp,
    ResetPassword
} from '../../screens'

const Stack = createStackNavigator();

function LoginNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
            // initialRouteName={'FinishSigningUp'}
        >
            <Stack.Screen name="GetStarted" component={GetStarted} />
            <Stack.Screen name="ChooseSignInUp" component={ChooseSignInUp} />
            <Stack.Screen name="SignIn" component={SignIn} />
            <Stack.Screen name="SignUp" component={SignUp} />
            <Stack.Screen name="ResetPassword" component={ResetPassword} />
        </Stack.Navigator>
    );
}

export default LoginNavigator;
