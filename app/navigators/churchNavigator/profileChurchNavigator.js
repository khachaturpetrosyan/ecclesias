import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    ProfileChurch,
} from '../../screens'

const Stack = createStackNavigator();

function ProfileChurchNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="ProfileChurch" component={ProfileChurch}/>
        </Stack.Navigator>
    );
}

export default ProfileChurchNavigator;
