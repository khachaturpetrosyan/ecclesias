import * as React from 'react';
import { Platform } from 'react-native'
import {Image, Text, Dimensions} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeChurchNavigator from './homeNavigator'
import ProfileChurchNavigator from './profileChurchNavigator'


const Tab = createBottomTabNavigator();


let getTabBarBusinessVisibility = (route) => {
    const routeName = route.state
        ? route.state.routes[route.state.index].name
        : '';

    return true;
}


export default function TabChurchNavigator() {
    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                unmountOnBlur: true,
                tabBarIcon: ({focused, color, size}) => {
                    let imageSource = null
                    if (route.name === 'HomeChurchNavigator') {
                        imageSource = require('../../assets/img/home_icon.png')
                    }
                    if (route.name === 'ChatNavigator') {
                        imageSource = require('../../assets/img/chat.png')
                    }
                    if (route.name === 'LocationNavigator') {
                        imageSource = require('../../assets/img/location.png')
                    }
                    if (route.name === 'ProfileChurchNavigator') {
                        imageSource = require('../../assets/img/profile.png')
                    }
                    return <Image
                        style={{
                            height: 25.2,
                            width: 25.2,
                            resizeMode: 'contain',
                            tintColor: color
                        }}
                        source={imageSource}
                    />;
                },
                tabBarLabel: ({focused, color, size}) => {
                },
            })}
            tabBarOptions={{
                activeTintColor: '#00add9',
                inactiveTintColor: '#9f9f9f',
                style: {
                    position: 'absolute',
                    height: 50,
                },
                tabStyle: {
                    height: 50,
                }
            }}
        >
            <Tab.Screen
                name="HomeChurchNavigator" component={HomeChurchNavigator}
                options={({route}) => ({
                    tabBarVisible: getTabBarBusinessVisibility(route)
                })}/>
            <Tab.Screen name="ProfileChurchNavigator" component={ProfileChurchNavigator}/>
{/*
                <Tab.Screen name="ChatNavigator" component={ChatNavigator}/>
                <Tab.Screen name="LocationNavigator" component={LocationNavigator}/>
                */}
        </Tab.Navigator>
    );
}
