import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    Location
} from '../../screens'

const Stack = createStackNavigator();

function LocationNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Location" component={Location}/>
        </Stack.Navigator>
    );
}

export default LocationNavigator;
