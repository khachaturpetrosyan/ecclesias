import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    HomeChurch,
    PostInfo,
    EditPost
} from '../../screens'

const Stack = createStackNavigator();

function HomeChurchNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="HomeChurch" component={HomeChurch}/>
            <Stack.Screen name="PostInfo" component={PostInfo}/>
            <Stack.Screen name="EditPost" component={EditPost}/>
        </Stack.Navigator>
    );
}

export default HomeChurchNavigator;
