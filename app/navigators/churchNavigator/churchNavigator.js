import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  Hello
} from '../../screens'
import TabChurchNavigator from "./tabChurchNavigator";

const Stack = createStackNavigator();

function ChurchNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Hello" component={Hello} />
            <Stack.Screen name="TabChurchNavigator" component={TabChurchNavigator} />
        </Stack.Navigator>
    );
}

export default ChurchNavigator;
