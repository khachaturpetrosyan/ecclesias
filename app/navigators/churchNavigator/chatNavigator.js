import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    Chat
} from '../../screens'

const Stack = createStackNavigator();

function ChatNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Chat" component={Chat}/>
        </Stack.Navigator>
    );
}

export default ChatNavigator;
