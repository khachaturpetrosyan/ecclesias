import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    Splash,
} from '../screens'
import LoginNavigator from './loginNavigator/loginNavigator';
import CustomerNavigator from './customerNavigator/customerNavigator';
import ChurchNavigator from './churchNavigator/churchNavigator';


const Stack = createStackNavigator();

function MainNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Splash" component={Splash}/>
            <Stack.Screen name="LoginNavigator" component={LoginNavigator}/>
            <Stack.Screen name="CustomerNavigator" component={CustomerNavigator}/>
            <Stack.Screen name="ChurchNavigator" component={ChurchNavigator}/>
        </Stack.Navigator>
    );
}

export default MainNavigator;
