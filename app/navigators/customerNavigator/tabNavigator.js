import * as React from 'react';
import { Image, Text, Dimensions } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { connect } from 'react-redux';
import HomeNavigator from './homeNavigator'
import ChatNavigator from './chatNavigator'
import LocationNavigator from './locationNavigator'
import ChurchCustomerNavigator from './churchCustomerNavigator'
import ProfileNavigator from './profileNavigator'


const Tab = createBottomTabNavigator();

let getTabBarChatVisibility = (route) => {
    const routeName = route.state
        ? route.state.routes[route.state.index].name
        : '';
    if (routeName === 'Chat' || routeName === 'ChatTest') {
        return false
    }
    return true;
}


 function TabNavigator(props) {

     return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                unmountOnBlur: true,
                tabBarIcon: ({focused, color, size}) => {
                    let imageSource = null
                    if (route.name === 'HomeNavigator') {
                        imageSource = require('../../assets/img/home_icon.png')
                    }
                    if (route.name === 'ChatNavigator') {
                        imageSource = require('../../assets/img/chat.png')
                    }
                    if (route.name === 'LocationNavigator') {
                        imageSource = require('../../assets/img/location.png')
                    }
                    if (route.name === 'ChurchCustomerNavigator') {
                        imageSource = require('../../assets/img/church_tab.png')
                    }
                    if (route.name === 'ProfileNavigator') {
                        imageSource = require('../../assets/img/profile.png')
                    }
                    return <Image
                        style={{
                            height: 25.2,
                            width: 25.2,
                            resizeMode: 'contain',
                            tintColor: color
                        }}
                        source={imageSource}
                    />;
                },
                tabBarLabel: ({focused, color, size}) => {
                },
            })}
            tabBarOptions={{
                activeTintColor: '#00add9',
                inactiveTintColor: '#9f9f9f',
                style: {
                    position: 'absolute',
                    height: 50,
                },
                tabStyle: {
                    height: 50,
                }
            }}
        >
            <Tab.Screen name="HomeNavigator" component={HomeNavigator}/>
            <Tab.Screen name="ChatNavigator" component={ChatNavigator}
                        options={
                            ({route}) => ({
                                tabBarBadge: props.customer.new_chat,
                                tabBarVisible: getTabBarChatVisibility(route)
                            })
                        }/>
            <Tab.Screen name="LocationNavigator" component={LocationNavigator}/>
            <Tab.Screen name="ChurchCustomerNavigator" component={ChurchCustomerNavigator}/>
            <Tab.Screen name="ProfileNavigator" component={ProfileNavigator}/>
        </Tab.Navigator>
    );
}

export default connect(({customer}) => ({customer}))(TabNavigator)