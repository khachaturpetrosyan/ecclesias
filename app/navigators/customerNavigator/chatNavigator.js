import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    ChatList,
    Chat,
} from '../../screens'

const Stack = createStackNavigator();

function ChatNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="ChatList" component={ChatList}/>
            <Stack.Screen name="Chat" component={Chat}/>
        </Stack.Navigator>
    );
}

export default ChatNavigator;
