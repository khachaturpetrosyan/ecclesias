import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  Hello
} from '../../screens'
import TabNavigator from './tabNavigator';

const Stack = createStackNavigator();

function CustomerNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Hello" component={Hello} />
            <Stack.Screen name="TabNavigator" component={TabNavigator} />
        </Stack.Navigator>
    );
}

export default CustomerNavigator;
