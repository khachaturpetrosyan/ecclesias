import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    Church
} from '../../screens'

const Stack = createStackNavigator();

function ChurchCustomerNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Church" component={Church}/>
        </Stack.Navigator>
    );
}

export default ChurchCustomerNavigator;
