import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
    Home,
    PostInfo
} from '../../screens'

const Stack = createStackNavigator();

function HomeNavigator() {
    return (
        <Stack.Navigator
            headerMode={'none'}
        >
            <Stack.Screen name="Home" component={Home}/>
            <Stack.Screen name="PostInfo" component={PostInfo}/>
        </Stack.Navigator>
    );
}

export default HomeNavigator;
