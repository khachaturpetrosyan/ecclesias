import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import MyApp from './app/myApp'
import store from "./app/store";
import {Provider} from "react-redux";

export default function App() {
    return (

        <NavigationContainer>
            <Provider store={store}>
                <MyApp/>
            </Provider>
        </NavigationContainer>
    );
}
